#include "helper.h"


std::wstring zyA2W(const char* pszSrc, int len /*= -1*/, int codePage /*= 936*/)
{
	int nSize = MultiByteToWideChar(codePage, 0, pszSrc, len, NULL, 0);
	if (nSize<=0)
	{
		return std::wstring();
	}
	wchar_t* buffer = new wchar_t[nSize + 1];
	
	nSize = MultiByteToWideChar(codePage, 0, pszSrc, len, buffer, nSize);
	if (nSize <= 0)
	{
		delete[] buffer;
		return std::wstring();
	}
	std::wstring retStr(buffer);
	delete[] buffer;
	return retStr;
}

std::string zyW2A(const wchar_t* pszwSrc, int len /*= -1*/, int codePage /*= 936*/)
{
	int nSize = WideCharToMultiByte(codePage, 0, pszwSrc, len, NULL, 0, NULL, NULL);
	if (nSize <= 0)
	{
		return std::string();
	}
	char* buffer = new char[nSize + 1];

	nSize = WideCharToMultiByte(codePage, 0, pszwSrc, len, buffer, nSize, NULL, NULL);
	if (nSize <= 0)
	{
		delete[] buffer;
		return std::string();
	}
	std::string retStr(buffer);
	delete[] buffer;
	return retStr;
}

std::string zyCodeConvert(const char* pszSrc, int len /*= -1*/, int codePageSrc /*= 936*/, int codePageDes /*= 65001*/)
{
	int nSize = MultiByteToWideChar(codePageSrc, 0, pszSrc, len, NULL, 0);
	if (nSize <= 0)
	{
		return std::string();
	}
	wchar_t* pszwStr = new wchar_t[nSize + 1];
	ZeroMemory(pszwStr, (nSize + 1) * sizeof(wchar_t));
	if (!pszwStr)
	{
		return std::string();
	}
	nSize = MultiByteToWideChar(codePageSrc, 0, pszSrc, len, pszwStr, nSize);
	if (nSize <= 0)
	{
		delete[] pszwStr;
		return std::string();
	}
	int nSizeRet = WideCharToMultiByte(codePageDes, 0, pszwStr, nSize, NULL, 0, NULL, NULL);
	if (nSizeRet <= 0)
	{
		delete[] pszwStr;
		return std::string();
	}
	char* buffer = new char[nSizeRet + 1];
	ZeroMemory(buffer, (nSizeRet + 1) * sizeof(char));
	nSizeRet = WideCharToMultiByte(codePageDes, 0, pszwStr, nSize, buffer, nSizeRet, NULL, NULL);
	if (nSizeRet <= 0)
	{
		delete[] pszwStr;
		delete[] buffer;
		return std::string();
	}
	std::string retStr(buffer);
	delete[] pszwStr;
	delete[] buffer;
	return retStr;
}




char* zyCloneToGB2312(const char* utf8)
{
	int nSize = MultiByteToWideChar(65001, 0, utf8, -1, NULL, 0);
	if (nSize <= 0)
	{
		return NULL;
	}
	wchar_t* pszwStr = new wchar_t[nSize + 1];
	if (!pszwStr)
	{
		return NULL;
	}
	nSize = MultiByteToWideChar(65001, 0, utf8, -1, pszwStr, nSize);
	if (nSize <= 0)
	{
		delete[] pszwStr;
		return NULL;
	}
	int nSizeRet = WideCharToMultiByte(936, 0, pszwStr, nSize, NULL, 0, NULL, NULL);
	if (nSizeRet <= 0)
	{
		delete[] pszwStr;
		return NULL;
	}
	char* buffer = (char*)esqlite31_MMalloc(nSizeRet + 1);
	nSizeRet = WideCharToMultiByte(936, 0, pszwStr, nSize, buffer, nSizeRet, NULL, NULL);
	if (nSizeRet <= 0)
	{
		delete[] pszwStr;
		esqlite31_MFree(buffer);
		return NULL;
	}
	
	delete[] pszwStr;

	return buffer;
}

std::string zyAllToUTF8(PMDATA_INF pArgInf)
{
	if (pArgInf->m_dtDataType == SDT_TEXT)
	{
		return zyCodeConvert(pArgInf->m_pText);
	}
	else
	{
		int len = (int)*((LPINT)(pArgInf->m_pBin + 4));
		return zyW2A((wchar_t*)(pArgInf->m_pBin + 8), len, 65001);
	}
}

int zyGetColumnIndex(sqlite3_stmt* hStmt, PMDATA_INF pArgInf)
{
	if (pArgInf->m_dtDataType == SDT_TEXT)
	{
		int count = sqlite3_column_count(hStmt);
		for (int i = 0; i < count; ++i)
		{
			std::string str = zyCodeConvert(sqlite3_column_name(hStmt, i), -1, 65001, 936);
			if (str == pArgInf->m_pText)
			{
				return i;
			}
		}
		return -1;
	}
	else if(pArgInf->m_dtDataType == SDT_INT)
	{
		return pArgInf->m_int;
	}
	else if (pArgInf->m_dtDataType == SDT_DOUBLE)
	{
		return (INT)pArgInf->m_double;
	}
	else if (pArgInf->m_dtDataType == SDT_FLOAT)
	{
		return (INT)pArgInf->m_float;
	}
	else
	{
		return pArgInf->m_int;
	}
}

std::string zyFieldTypeToString(int type)
{
	switch (type)
	{
	case 1:
		return "Byte";
	case 2:
		return "Smallint";
	case 3:
		return "Integer";
	case 4:
		return "BigInt";
	case 5:
		return "Decimal";
	case 6:
		return "Double";
	case 7:
		return "Bit";
	case 8:
		return "DateTime";
	case 10:
		return "VarChar";
	case 11:
		return "Blob";
	case 12:
		return "Text";
	}
	return "";
}

std::string zyFieldFlagsToString(int flags)
{
	std::string str;
	if (flags & 1) //SQLITE_PRIMARY_KEY
	{
		str = "Primary Key";
	}
	if (flags & 2) //SQLITE_AUTO_INCREMENT
	{
		str += " AutoIncrement";
	}
	if (flags & 4) //SQLITE_NOT_NULL
	{
		str += " Not Null";
	}
	if (flags & 8) //SQLITE_UNIQUE
	{
		str += " Unique";
	}
	return str;
}

void esqlite31_destructor(void* ptr)
{
	if (ptr)
	{
		esqlite31_MFree(ptr);
	}
}
