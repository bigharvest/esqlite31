#pragma once

#include <windows.h>
#include "string.h"
#include <algorithm>
#include <string>
#include "elib_sdk/lib2.h"
#include "sqlite3.h"
#include "elib_sdk/fnshare.h"

std::wstring zyA2W(const char* pszSrc, int len = -1, int codePage = 936);
std::string zyW2A(const wchar_t* pszwSrc, int len = -1, int codePage = 936);
std::string zyCodeConvert(const char* pszSrc, int len = -1, int codePageSrc = 936, int codePageDes = 65001);
char* zyCloneToGB2312(const char* utf8);

std::string zyAllToUTF8(PMDATA_INF pArgInf);
int zyGetColumnIndex(sqlite3_stmt* hStmt, PMDATA_INF pArgInf);
std::string zyFieldTypeToString(int type);
std::string zyFieldFlagsToString(int flags);
void esqlite31_destructor(void* ptr);

class CFreqMem
{
private:
	unsigned char* m_pdata;
	size_t m_datalen, m_bufferlen;
public:
	CFreqMem()
	{
		m_pdata = NULL; m_datalen = m_bufferlen = 0;
	}
	void* GetPtr()
	{
		return (m_datalen == 0 ? NULL : m_pdata);
	}
	void AddDWord(DWORD dw)
	{
		AppendData(&dw, sizeof(dw));
	}
	void AppendData(void* pdata, size_t size)
	{
		if (m_bufferlen - m_datalen < size)
		{
			if (m_pdata == NULL)
			{
				m_bufferlen = 128;
				m_pdata = (unsigned char*)esqlite31_MMalloc(m_bufferlen);
				//assert(m_datalen == 0);
			}
			size_t oldLen = m_bufferlen;
			while (m_bufferlen - m_datalen < size)
			{
				m_bufferlen *= 2;
			};

			if (oldLen < m_bufferlen)
			{
				unsigned char* newData = (unsigned char*)esqlite31_MMalloc(m_bufferlen);
				memcpy(newData, m_pdata, oldLen);
				esqlite31_MFree(m_pdata);
				m_pdata = newData;
			}
			
		}
		memcpy(m_pdata + m_datalen, pdata, size);
		m_datalen += size;
	}
};