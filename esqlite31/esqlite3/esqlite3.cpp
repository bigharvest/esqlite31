// esqlite3.cpp: 定义 DLL 应用程序的导出函数。
//

#include "esqlite3.h"

#include "elib_sdk/lib2.h"
#include "elib_sdk/lang.h"
#include "elib_sdk/fnshare.h"
#include "elib_sdk/fnshare.cpp"
#include <string.h>


/************************************************************************/
/* 常量定义
/************************************************************************/
#ifndef __E_STATIC_LIB
LIB_CONST_INFO s_ConstInfo[] =
{
	/* { 中文名称, 英文名称, 常量说明, 常量等级(LVL_), 参数类型(CT_), 文本内容, 数值内容 }   只有两种数据类型*/
	{ _WT("SQLITE_成功"), _WT("SQLITE_OK"), NULL, LVL_SIMPLE, CT_NUM, NULL, SQLITE_OK },
	{ _WT("SQLITE_错误"), _WT("SQLITE_ERROR"), _WT("一般错误"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_ERROR },
	{ _WT("SQLITE_内部错误"), _WT("SQLITE_INTERNAL"), _WT("SQLite中的内部逻辑错误"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_INTERNAL },
	{ _WT("SQLITE_权限"), _WT("SQLITE_PERM"), _WT("访问权限被拒绝"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_PERM },
	{ _WT("SQLITE_终止"), _WT("SQLITE_ABORT"), _WT("回滚事务请求中止"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_ABORT },
	{ _WT("SQLITE_繁忙"), _WT("SQLITE_BUSY"), _WT("数据库文件已锁定"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_BUSY },
	{ _WT("SQLITE_锁定"), _WT("SQLITE_LOCKED"), _WT("数据库中的表已锁定"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_LOCKED },
	{ _WT("SQLITE_内存不足"), _WT("SQLITE_NOMEM"), _WT("分配内存失败"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_NOMEM },
	{ _WT("SQLITE_只读"), _WT("SQLITE_READONLY"), _WT("尝试写入只读数据库"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_READONLY },
	{ _WT("SQLITE_中断"), _WT("SQLITE_INTERRUPT"), NULL, LVL_SIMPLE, CT_NUM, NULL, SQLITE_INTERRUPT },
	{ _WT("SQLITE_IO错误"), _WT("SQLITE_IOERR"), _WT("发生某种磁盘I / O错误"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_IOERR },
	{ _WT("SQLITE_损坏"), _WT("SQLITE_CORRUPT"), _WT("数据库磁盘映像格式错误"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_CORRUPT },
	{ _WT("SQLITE_未找到"), _WT("SQLITE_NOTFOUND"),  _WT(""), LVL_SIMPLE, CT_NUM, NULL, SQLITE_NOTFOUND },
	{ _WT("SQLITE_已满"), _WT("SQLITE_FULL"), _WT("插入失败，因为数据库已满"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_FULL },
	{ _WT("SQLITE_不能打开"), _WT("SQLITE_CANTOPEN"), _WT("无法打开数据库文件"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_CANTOPEN },
	{ _WT("SQLITE_协议"), _WT("SQLITE_PROTOCOL"), _WT("数据库锁协议错误"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_PROTOCOL },
	{ _WT("SQLITE_空"), _WT("SQLITE_EMPTY"), _WT("限内部使用"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_EMPTY },
	{ _WT("SQLITE_结构"), _WT("SQLITE_SCHEMA"), _WT("数据库结构被改变"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_SCHEMA },
	{ _WT("SQLITE_太大"), _WT("SQLITE_TOOBIG"), _WT("文本或字节集超出大小限制"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_TOOBIG },
	{ _WT("SQLITE_约束"), _WT("SQLITE_CONSTRAINT"), _WT("由于约束冲突而中止"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_CONSTRAINT },
	{ _WT("SQLITE_不匹配"), _WT("SQLITE_MISMATCH"), _WT("数据类型不匹配"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_MISMATCH },
	{ _WT("SQLITE_误用"), _WT("SQLITE_MISUSE"), _WT("库被不正确使用"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_MISUSE },
	{ _WT("SQLITE_系统不支持"), _WT("SQLITE_NOLFS"), _WT("使用主机不支持的操作系统功能"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_NOLFS },
	{ _WT("SQLITE_认证"), _WT("SQLITE_AUTH"), _WT("授权被拒绝"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_AUTH },
	{ _WT("SQLITE_格式"), _WT("SQLITE_FORMAT"), _WT("未使用"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_FORMAT },
	{ _WT("SQLITE_范围"), _WT("SQLITE_RANGE"), _WT("绑定参数时索引超出范围"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_RANGE },
	{ _WT("SQLITE_不是数据库"), _WT("SQLITE_NOTADB"), _WT("打开的文件不是数据库文件"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_NOTADB },
	{ _WT("SQLITE_通知"), _WT("SQLITE_NOTICE"), _WT("来自日志的通知"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_NOTICE },
	{ _WT("SQLITE_警告"), _WT("SQLITE_WARNING"), _WT("来自日志的警告"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_WARNING },
	{ _WT("SQLITE_行"), _WT("SQLITE_ROW"), _WT("记录集有下一行"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_ROW },
	{ _WT("SQLITE_完成"), _WT("SQLITE_DONE"), _WT("记录集完成执行"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_DONE },

	{ _WT("SQLITE_配置_单线程"), _WT("SQLITE_CONFIG_SINGLETHREAD"), _WT("无参数"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_CONFIG_SINGLETHREAD },
	{ _WT("SQLITE_配置_多线程"), _WT("SQLITE_CONFIG_MULTITHREAD"), _WT("无参数，使用互斥锁，各个线程使用不同的连接和准备的SQL语句"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_CONFIG_MULTITHREAD },
	{ _WT("SQLITE_配置_串行化"), _WT("SQLITE_CONFIG_SERIALIZED"), _WT("无参数，使用所有互斥锁，多个线程都可以使用相同的连接和准备的SQL语句"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_CONFIG_SERIALIZED },
	{ _WT("SQLITE_配置_内存状态"), _WT("SQLITE_CONFIG_MEMSTATUS"), _WT("逻辑参数 启用或禁用内存分配统计信息"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_CONFIG_MEMSTATUS },
	{ _WT("SQLITE_配置_日志"), _WT("SQLITE_CONFIG_LOG"), _WT("全局错误日志，第一个参数 void(*)(void*,int,const char*)，第二个参数 回调函数的第一个参数"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_CONFIG_LOG },
	{ _WT("SQLITE_配置_URI"), _WT("SQLITE_CONFIG_URI"), _WT("逻辑参数 启用或禁用URI处理"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_CONFIG_URI },
	{ _WT("SQLITE_配置_覆盖索引扫描"), _WT("SQLITE_CONFIG_COVERING_INDEX_SCAN"), _WT("逻辑参数 启用或禁用查询优化器中覆盖索引以进行全表扫描"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_CONFIG_COVERING_INDEX_SCAN },

	{ _WT("SQLITE_打开_只读"), _WT("SQLITE_OPEN_READONLY"), _WT(""), LVL_SIMPLE, CT_NUM, NULL, SQLITE_OPEN_READONLY },
	{ _WT("SQLITE_打开_读写"), _WT("SQLITE_OPEN_READWRITE"), _WT(""), LVL_SIMPLE, CT_NUM, NULL, SQLITE_OPEN_READWRITE },
	{ _WT("SQLITE_打开_创建"), _WT("SQLITE_OPEN_CREATE"), _WT(""), LVL_SIMPLE, CT_NUM, NULL, SQLITE_OPEN_CREATE },
	/*{ _WT("SQLITE_打开_DELETEONCLOSE"), _WT("SQLITE_OPEN_DELETEONCLOSE"), _WT("仅VFS"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_OPEN_DELETEONCLOSE },
	{ _WT("SQLITE_打开_EXCLUSIVE"), _WT("SQLITE_OPEN_EXCLUSIVE"), _WT("仅VFS"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_OPEN_EXCLUSIVE },
	{ _WT("SQLITE_打开_AUTOPROXY"), _WT("SQLITE_OPEN_AUTOPROXY"), _WT("仅VFS"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_OPEN_AUTOPROXY },*/
	{ _WT("SQLITE_打开_URI"), _WT("SQLITE_OPEN_URI"), _WT("支持URI"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_OPEN_URI },
	{ _WT("SQLITE_打开_内存"), _WT("SQLITE_OPEN_MEMORY"), _WT(""), LVL_SIMPLE, CT_NUM, NULL, SQLITE_OPEN_MEMORY },
	/*{ _WT("SQLITE_打开_MAIN_DB"), _WT("SQLITE_OPEN_MAIN_DB"), _WT("仅VFS"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_OPEN_MAIN_DB },
	{ _WT("SQLITE_打开_TEMP_DB"), _WT("SQLITE_OPEN_TEMP_DB"), _WT("仅VFS"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_OPEN_TEMP_DB },
	{ _WT("SQLITE_打开_TRANSIENT_DB"), _WT("SQLITE_OPEN_TRANSIENT_DB"), _WT("仅VFS"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_OPEN_TRANSIENT_DB },
	{ _WT("SQLITE_打开_MAIN_JOURNAL"), _WT("SQLITE_OPEN_MAIN_JOURNAL"), _WT("仅VFS"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_OPEN_MAIN_JOURNAL },
	{ _WT("SQLITE_打开_TEMP_JOURNAL"), _WT("SQLITE_OPEN_TEMP_JOURNAL"), _WT("仅VFS"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_OPEN_TEMP_JOURNAL },
	{ _WT("SQLITE_打开_SUBJOURNAL"), _WT("SQLITE_OPEN_SUBJOURNAL"), _WT("仅VFS"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_OPEN_SUBJOURNAL },
	{ _WT("SQLITE_打开_MASTER_JOURNAL"), _WT("SQLITE_OPEN_MASTER_JOURNAL"), _WT("仅VFS"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_OPEN_MASTER_JOURNAL },*/
	{ _WT("SQLITE_打开_无互斥"), _WT("SQLITE_OPEN_NOMUTEX"), _WT(""), LVL_SIMPLE, CT_NUM, NULL, SQLITE_OPEN_NOMUTEX },
	{ _WT("SQLITE_打开_全互斥"), _WT("SQLITE_OPEN_FULLMUTEX"), _WT(""), LVL_SIMPLE, CT_NUM, NULL, SQLITE_OPEN_FULLMUTEX },
	{ _WT("SQLITE_打开_共享缓存"), _WT("SQLITE_OPEN_SHAREDCACHE"), _WT(""), LVL_SIMPLE, CT_NUM, NULL, SQLITE_OPEN_SHAREDCACHE },
	{ _WT("SQLITE_打开_私有缓存"), _WT("SQLITE_OPEN_PRIVATECACHE"), _WT(""), LVL_SIMPLE, CT_NUM, NULL, SQLITE_OPEN_PRIVATECACHE },
	//{ _WT("SQLITE_打开_WAL"), _WT("SQLITE_OPEN_WAL"), _WT("VFS"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_OPEN_WAL },

	{ _WT("SQLITE_类型_整数型"), _WT("SQLITE_INTEGER"), _WT(""), LVL_SIMPLE, CT_NUM, NULL, SQLITE_INTEGER },
	{ _WT("SQLITE_类型_浮点型"), _WT("SQLITE_FLOAT"), _WT(""), LVL_SIMPLE, CT_NUM, NULL, SQLITE_FLOAT },
	{ _WT("SQLITE_类型_文本型"), _WT("SQLITE_TEXT"), _WT(""), LVL_SIMPLE, CT_NUM, NULL, SQLITE_TEXT },
	{ _WT("SQLITE_类型_字节集型"), _WT("SQLITE_BLOB"), _WT(""), LVL_SIMPLE, CT_NUM, NULL, SQLITE_BLOB },
	{ _WT("SQLITE_类型_空"), _WT("SQLITE_NULL"), _WT(""), LVL_SIMPLE, CT_NUM, NULL, SQLITE_NULL },

	{ _WT("SQLITE_字段属性_主键"), _WT("SQLITE_PRIMARY_KEY"), _WT("Primary Key"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_PRIMARY_KEY },
	{ _WT("SQLITE_字段属性_自动递增"), _WT("SQLITE_AUTO_INCREMENT"), _WT("AutoIncrement"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_AUTO_INCREMENT },
	{ _WT("SQLITE_字段属性_非空"), _WT("SQLITE_NOT_NULL"), _WT("Not Null"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_NOT_NULL },
	{ _WT("SQLITE_字段属性_唯一"), _WT("SQLITE_UNIQUE"), _WT("Unique"), LVL_SIMPLE, CT_NUM, NULL, SQLITE_UNIQUE },

	{ _WT("wxCODEC_TYPE_UNKNOWN"), _WT("CODEC_TYPE_UNKNOWN"), _WT(""), LVL_SIMPLE, CT_NUM, NULL, CODEC_TYPE_UNKNOWN },
	{ _WT("wxCODEC_TYPE_AES128"), _WT("CODEC_TYPE_AES128"), _WT(""), LVL_SIMPLE, CT_NUM, NULL, CODEC_TYPE_AES128 },
	{ _WT("wxCODEC_TYPE_AES256"), _WT("CODEC_TYPE_AES256"), _WT(""), LVL_SIMPLE, CT_NUM, NULL, CODEC_TYPE_AES256 },
	{ _WT("wxCODEC_TYPE_CHACHA20"), _WT("CODEC_TYPE_CHACHA20"), _WT(""), LVL_SIMPLE, CT_NUM, NULL, CODEC_TYPE_CHACHA20 },
	{ _WT("wxCODEC_TYPE_SQLCIPHER"), _WT("CODEC_TYPE_SQLCIPHER"), _WT(""), LVL_SIMPLE, CT_NUM, NULL, CODEC_TYPE_SQLCIPHER },

	{ _WT("wxSQLITE_Cipher_Global"), _WT("global"), _WT(""), LVL_SIMPLE, CT_TEXT, _WT("global"), 0 },
	{ _WT("wxSQLITE_Cipher_AES128"), _WT("aes128cbc"), _WT(""), LVL_SIMPLE, CT_TEXT, _WT("aes128cbc"), 0 },
	{ _WT("wxSQLITE_Cipher_AES256"), _WT("aes256cbc"), _WT(""), LVL_SIMPLE, CT_TEXT, _WT("aes256cbc"), 0 },
	{ _WT("wxSQLITE_Cipher_CHACHA20"), _WT("chacha20"), _WT(""), LVL_SIMPLE, CT_TEXT, _WT("chacha20"), 0 },
	{ _WT("wxSQLITE_Cipher_SQLCIPHER"), _WT("sqlcipher"), _WT(""), LVL_SIMPLE, CT_TEXT, _WT("sqlcipher"), 0 },

	{ _WT("wxSQLITE_Config_cipher"), _WT("cipher"), _WT("[全局]“wxCODEC_TYPE_”开头常量，可用于配置加密算法"), LVL_SIMPLE, CT_TEXT, _WT("cipher"), 0 },
	{ _WT("wxSQLITE_Config_legacy"), _WT("legacy"), _WT("[AES128，AES256，CHACHA20，SQLCIPHER]，0或1"), LVL_SIMPLE, CT_TEXT, _WT("legacy"), 0 },
	{ _WT("wxSQLITE_Config_legacy_page_size"), _WT("legacy_page_size"), _WT("[AES128，AES256，CHACHA20，SQLCIPHER]，最大65536"), LVL_SIMPLE, CT_TEXT, _WT("legacy_page_size"), 0 },
	{ _WT("wxSQLITE_Config_kdf_iter"), _WT("kdf_iter"), _WT("[AES256，CHACHA20，SQLCIPHER]"), LVL_SIMPLE, CT_TEXT, _WT("kdf_iter"), 0 },
	{ _WT("wxSQLITE_Config_fast_kdf_iter"), _WT("fast_kdf_iter"), _WT("[SQLCIPHER]"), LVL_SIMPLE, CT_TEXT, _WT("fast_kdf_iter"), 0 },
	{ _WT("wxSQLITE_Config_hmac_use"), _WT("hmac_use"), _WT("[SQLCIPHER]"), LVL_SIMPLE, CT_TEXT, _WT("hmac_use"), 0 },
	{ _WT("wxSQLITE_Config_hmac_pgno"), _WT("hmac_pgno"), _WT("[SQLCIPHER]"), LVL_SIMPLE, CT_TEXT, _WT("hmac_pgno"), 0 },
	{ _WT("wxSQLITE_Config_hmac_salt_mask"), _WT("hmac_salt_mask"), _WT("[SQLCIPHER]"), LVL_SIMPLE, CT_TEXT, _WT("hmac_salt_mask"), 0 },

	{ _WT("SQLITE3_触发条件_插入"), _WT("INSERT"), _WT(""), LVL_SIMPLE, CT_NUM, NULL, 0 },
	{ _WT("SQLITE3_触发条件_删除"), _WT("DELETE"), _WT(""), LVL_SIMPLE, CT_NUM, NULL, 1 },
	{ _WT("SQLITE3_触发条件_更新"), _WT("UPDATE"), _WT(""), LVL_SIMPLE, CT_NUM, NULL, 2 },
	{ _WT("SQLITE3_触发条件_字段更新"), _WT("UPDATE OF"), _WT(""), LVL_SIMPLE, CT_NUM, NULL, 3 },

	{ _WT("SQLITE3_触发时间_普通模式"), _WT(""), _WT(""), LVL_SIMPLE, CT_NUM, NULL, 0 },
	{ _WT("SQLITE3_触发时间_之前"), _WT("BEFORE"), _WT(""), LVL_SIMPLE, CT_NUM, NULL, 1 },
	{ _WT("SQLITE3_触发时间_之后"), _WT("AFTER"), _WT(""), LVL_SIMPLE, CT_NUM, NULL, 2 },

	{ _WT("SQLITE3_事务锁状态_推迟"), _WT("DEFERRED"), _WT("事务不获取任何锁(直到它需要锁的时候)"), LVL_SIMPLE, CT_NUM, NULL, 0 },
	{ _WT("SQLITE3_事务锁状态_立即"), _WT("IMMEDIATE"), _WT("事务会尝试获取RESERVED锁。如果成功，保证没有别的连接可以写数据库。但是，别的连接可以对数据库进行读操作"), LVL_SIMPLE, CT_NUM, NULL, 1 },
	{ _WT("SQLITE3_事务锁状态_独占"), _WT("EXCLUSIVE"), _WT("事务会试着获取对数据库的EXCLUSIVE锁。如果成功，别的连接无法读写数据库。"), LVL_SIMPLE, CT_NUM, NULL, 2 },

	{ _WT("SQLITE_编码_UTF8"), _WT("SQLITE_UTF8"), _WT(""), LVL_SIMPLE, CT_NUM, NULL, 1 },
	{ _WT("SQLITE_编码_UTF16LE"), _WT("SQLITE_UTF16LE"), _WT(""), LVL_SIMPLE, CT_NUM, NULL, 2 },
	{ _WT("SQLITE_编码_UTF16BE"), _WT("SQLITE_UTF16BE"), _WT(""), LVL_SIMPLE, CT_NUM, NULL, 3 },
	{ _WT("SQLITE_编码_UTF16"), _WT("SQLITE_UTF16"), _WT(""), LVL_SIMPLE, CT_NUM, NULL, 4 },
	{ _WT("SQLITE_编码_ANY"), _WT("SQLITE_ANY"), _WT(""), LVL_SIMPLE, CT_NUM, NULL, 5 },
	{ _WT("SQLITE_编码_UTF16_ALIGNED"), _WT("SQLITE_UTF16_ALIGNED"), _WT(""), LVL_SIMPLE, CT_NUM, NULL, 8 },
};

#endif


#ifndef __E_STATIC_LIB

ARG_INFO s_arg_to_gb2312[] =
{
	{ _WT("文本指针"), _WT(""),0,0, SDT_INT, 0, NULL },
};

// 参数 
ARG_INFO s_arg_config[] =
{
	/* { 参数名称, 参数描述, 图像索引, 图像数量, 参数类型(参见SDT_), 默认数值, 参数类别(参见AS_) } */
	{ _WT("配置项"), _WT("“SQLITE_配置_”开头常量"),0,0, SDT_INT, 0, NULL },
	{ _WT("参数1"), _WT(""),0,0, SDT_INT, 0, AS_DEFAULT_VALUE_IS_EMPTY },
	{ _WT("参数2"), _WT(""),0,0, SDT_INT, 0, AS_DEFAULT_VALUE_IS_EMPTY },
};
ARG_INFO s_arg_open_v2[] =
{
	{ _WT("文件名"), _WT("数据库文件路径，如果为“:memory:”表示内存数据库；如果为“”表示临时数据库。"),0,0, _SDT_ALL, 0, NULL },
	{ _WT("打开选项"), _WT("“SQLITE_打开_”开头常量组合"),0,0, SDT_INT, SQLITE_OPEN_READWRITE, AS_HAS_DEFAULT_VALUE },
};

ARG_INFO s_arg_key_v2[] =
{
	{ _WT("密码"), _WT("数据库密码。只能传递文本或字节集"),0,0, _SDT_ALL, 0, AS_DEFAULT_VALUE_IS_EMPTY },
	{ _WT("数据库名"), _WT("指定数据库的名称"),0,0, SDT_TEXT, 0, AS_DEFAULT_VALUE_IS_EMPTY },
};

ARG_INFO s_arg_exec[] =
{
	{ _WT("SQL语句"), _WT("多条语句用;分开"),0,0, _SDT_ALL, 0, NULL },
	{ _WT("回调函数"), _WT("返回值：整数型（整数型 用户数据，整数型 列数量，整数型 数据指针，整数型 列名称指针）"),0,0, _SDT_ALL, 0, AS_DEFAULT_VALUE_IS_EMPTY },
	{ _WT("用户数据"), _WT("传递给回调函数的第一个参数"),0,0, SDT_INT, 0, AS_DEFAULT_VALUE_IS_EMPTY },
	{ _WT("错误信息"), _WT(""),0,0, SDT_TEXT, 0, AS_DEFAULT_VALUE_IS_EMPTY | AS_RECEIVE_VAR },
};

ARG_INFO s_arg_set_hdb[] =
{
	{ _WT("数据库句柄"), _WT(""),0,0, SDT_INT, 0, NULL },
};

ARG_INFO s_arg_prepare[] =
{
	{ _WT("SQL语句"), _WT(""),0,0, _SDT_ALL, 0, NULL },
	{ _WT("执行结果"), _WT(""),0,0, SDT_BOOL, 0, AS_DEFAULT_VALUE_IS_EMPTY | AS_RECEIVE_VAR },
};

ARG_INFO s_arg_begin_transaction[] =
{
	{ _WT("事务名称"), _WT(""),0,0, SDT_TEXT, 0, AS_DEFAULT_VALUE_IS_EMPTY },
	{ _WT("事务锁状态"), _WT("SQLITE3_事务锁状态_ 开头常量，如果提供了此参数，则事务名称无效。默认为：SQLITE3_事务锁状态_推迟"),0,0, SDT_INT, 0, AS_DEFAULT_VALUE_IS_EMPTY },
};
ARG_INFO s_arg_transaction[] =
{
	{ _WT("事务名称"), _WT(""),0,0, SDT_TEXT, 0, AS_DEFAULT_VALUE_IS_EMPTY },
};

ARG_INFO s_arg_table_name[] =
{
	{ _WT("表名"), _WT(""),0,0, SDT_TEXT, 0, NULL },
};
ARG_INFO s_arg_set_sql[] =
{
	{ _WT("SQL语句"), _WT(""),0,0, _SDT_ALL, 0, NULL },
	{ _WT("数据库"), _WT(""),0,0, DTP_SQLITE_DB, 0, NULL },
};
ARG_INFO s_arg_get_value[] =
{
	{ _WT("字段名称或位置"), _WT("指定欲读取的字段，可以为一个字段名称文本或者一个字段位置数值，字段位置数值从0开始。"),0,0, _SDT_ALL, 0, NULL },
	{ _WT("执行结果"), _WT("如果提供本参数，其中将存放本方法执行结果——成功为“真”，失败为“假”。"),0,0, SDT_BOOL, 0, AS_DEFAULT_VALUE_IS_EMPTY | AS_RECEIVE_VAR },
};
ARG_INFO s_arg_index[] =
{
	{ _WT("字段名称或位置"), _WT("指定欲读取的字段，可以为一个字段名称文本或者一个字段位置数值，字段位置数值从0开始。"),0,0, _SDT_ALL, 0, NULL },
};
ARG_INFO s_arg_get_any_value[] =
{
	{ _WT("字段名称或位置"), _WT("指定欲读取的字段，可以为一个字段名称文本或者一个字段位置数值，字段位置数值从0开始。"),0,0, _SDT_ALL, 0, NULL },
	{ _WT("字段值"), _WT("其中将存放取得的字段值。如果必要，将自动进行适当的数据类型转换。"),0,0, _SDT_ALL, 0, AS_RECEIVE_VAR },
};
ARG_INFO s_arg_bind[] =
{
	{ _WT("参数名称或位置"), _WT("使用参数名称时注意，“:”“$”“@”也是参数名称的一部分；使用参数索引时注意，索引从1开始。"),0,0, _SDT_ALL, 0, NULL },
	{ _WT("要绑定的值"), _WT("如果省略本参数，默认为null。"),0,0, _SDT_ALL, 0, AS_DEFAULT_VALUE_IS_EMPTY },
};
ARG_INFO s_arg_bind_parameter_name[] =
{
	{ _WT("参数索引"), _WT("索引从1开始。"),0,0, SDT_INT, 0, NULL },
};
ARG_INFO s_arg_get_table[] =
{
	{ _WT("SQL语句"), _WT(""),0,0, _SDT_ALL, 0, NULL },
	{ _WT("行数"), _WT("包含表头的行数"),0,0, SDT_INT, 0, AS_DEFAULT_VALUE_IS_EMPTY|AS_RECEIVE_VAR },
	{ _WT("列数"), _WT(""),0,0, SDT_INT, 0, AS_DEFAULT_VALUE_IS_EMPTY | AS_RECEIVE_VAR },
	{ _WT("错误信息"), _WT(""),0,0, SDT_TEXT, 0, AS_DEFAULT_VALUE_IS_EMPTY | AS_RECEIVE_VAR },
	{ _WT("执行结果"), _WT(""),0,0, SDT_BOOL, 0, AS_DEFAULT_VALUE_IS_EMPTY | AS_RECEIVE_VAR },
};

ARG_INFO s_arg_create_table[] =
{
	{ _WT("表名"), _WT(""),0,0, SDT_TEXT, 0, NULL },
	{ _WT("字段信息"), _WT(""),0,0, DTP_SQLITE_FIELD_INFO, 0, AS_RECEIVE_ARRAY_DATA },
	{ _WT("联合唯一"), _WT("字段名, 多个字段用“,”分开。如：“Column1 ACS,Column2 DESC”"),0,0, SDT_TEXT, 0, AS_DEFAULT_VALUE_IS_EMPTY },
	{ _WT("联合主键"), _WT("字段名, 多个字段用“,”分开。如：“Column1 ACS,Column2 DESC”"),0,0, SDT_TEXT, 0, AS_DEFAULT_VALUE_IS_EMPTY },
};

ARG_INFO s_arg_rename_table[] =
{
	{ _WT("源表名"), _WT(""),0,0, SDT_TEXT, 0, NULL },
	{ _WT("新表名"), _WT(""),0,0, SDT_TEXT, 0, NULL },
};

ARG_INFO s_arg_add_column[] =
{
	{ _WT("表名"), _WT(""),0,0, SDT_TEXT, 0, NULL },
	{ _WT("字段信息"), _WT(""),0,0, DTP_SQLITE_FIELD_INFO, 0, 0 },
};
ARG_INFO s_arg_create_index[] =
{
	{ _WT("索引名"), _WT(""),0,0, SDT_TEXT, 0, NULL },
	{ _WT("表名"), _WT(""),0,0, SDT_TEXT, 0, NULL },
	{ _WT("字段名"), _WT("多个字段用“,”分开。如：“Column1 ACS,Column2 DESC”"),0,0, SDT_TEXT, 0, NULL },
	{ _WT("唯一"), _WT(""),0,0, SDT_BOOL, 0, AS_HAS_DEFAULT_VALUE },
};
ARG_INFO s_arg_delete_index[] =
{
	{ _WT("索引名"), _WT(""),0,0, SDT_TEXT, 0, NULL },
};
ARG_INFO s_arg_create_view[] =
{
	{ _WT("视图名"), _WT(""),0,0, SDT_TEXT, 0, NULL },
	{ _WT("查询语句"), _WT("select 语句"),0,0, SDT_TEXT, 0, NULL },
	{ _WT("是否临时视图"), _WT(""),0,0, SDT_BOOL, 0, AS_HAS_DEFAULT_VALUE },
};
ARG_INFO s_arg_delete_view[] =
{
	{ _WT("视图名"), _WT(""),0,0, SDT_TEXT, 0, NULL },
};

ARG_INFO s_arg_create_trigger[] =
{
	{ _WT("触发器名"), _WT(""),0,0, SDT_TEXT, 0, NULL },
	{ _WT("表名"), _WT(""),0,0, SDT_TEXT, 0, NULL },
	{ _WT("执行语句"), _WT("当事件成立时，所触发的SQL语句。注意：必须以“;”号结尾"),0,0, SDT_TEXT, 0, NULL },
	{ _WT("触发条件"), _WT("“SQLITE3_触发条件_”开头常量。"),0,0, SDT_INT, 0, NULL },
	{ _WT("字段名"), _WT("当参数<触发条件> =“SQLITE3_触发条件_字段更新”时，指定字段名，多个字段用“,”分开"),0,0, SDT_TEXT, 0, AS_DEFAULT_VALUE_IS_EMPTY },
	{ _WT("触发时间"), _WT("“SQLITE3_触发时间_”开头常量。默认为“SQLITE3_触发时间_普通模式”"),0,0, SDT_INT, 0, AS_DEFAULT_VALUE_IS_EMPTY },
	{ _WT("是否临时触发器"), _WT(""),0,0, SDT_BOOL, 0, AS_HAS_DEFAULT_VALUE },
};

ARG_INFO s_arg_delete_trigger[] =
{
	{ _WT("触发器名"), _WT(""),0,0, SDT_TEXT, 0, NULL },
};

ARG_INFO s_arg_attach_db[] =
{
	{ _WT("文件名"), _WT("数据库文件路径"),0,0, _SDT_ALL, 0, NULL },
	{ _WT("数据库名"), _WT("以后可以通过这个数据库名，访问附加的数据库。如：“数据库名.表名”"),0,0, SDT_TEXT, 0, NULL },
	{ _WT("密码"), _WT("数据库密码。"),0,0, SDT_TEXT, 0, AS_DEFAULT_VALUE_IS_EMPTY },
};
ARG_INFO s_arg_detach_db[] =
{
	{ _WT("数据库名"), _WT("附加的数据库名"),0,0, SDT_TEXT, 0, NULL },
};

ARG_INFO s_arg_backup_db[] =
{
	{ _WT("目标数据库"), _WT("备份到此数据库"),0,0, DTP_SQLITE_DB, 0, NULL },
	{ _WT("目标数据库名"), _WT("备份到此数据库名，默认为：main 主数据库。"),0,0, SDT_TEXT, 0, AS_DEFAULT_VALUE_IS_EMPTY },
	{ _WT("源数据库名"), _WT("源数据库名，默认为：main 主数据库。"),0,0, SDT_TEXT, 0, AS_DEFAULT_VALUE_IS_EMPTY },
	{ _WT("回调函数"), _WT("无返回值 (整数型 剩余页数，整数型 总页数) "),0,0, _SDT_ALL, 0, AS_DEFAULT_VALUE_IS_EMPTY },
};

ARG_INFO s_arg_create_function[] =
{
	{ _WT("函数名"), _WT(""),0,0, SDT_TEXT, 0, NULL },
	{ _WT("参数个数"), _WT("如果为 -1 表示任意参数个数"),0,0, SDT_INT, 0, NULL },
	{ _WT("用户数据"), _WT("传递给析构函数的参数"),0,0, SDT_INT, 0, AS_HAS_DEFAULT_VALUE },
	{ _WT("实现函数"), _WT("无返回值 (整数型 sqlite3上下文，整数型 参数个数，整数型 参数指针)。"),0,0, _SDT_ALL, 0, NULL },
	{ _WT("析构函数"), _WT("无返回值 (整数型 用户数据)，当数据库关闭或函数被覆盖时调用此方法。"),0,0, _SDT_ALL, 0, AS_DEFAULT_VALUE_IS_EMPTY },
};

ARG_INFO s_arg_create_agg_function[] =
{
	{ _WT("函数名"), _WT(""),0,0, SDT_TEXT, 0, NULL },
	{ _WT("参数个数"), _WT("如果为 -1 表示任意参数个数"),0,0, SDT_INT, 0, NULL },
	{ _WT("用户数据"), _WT("传递给析构函数的参数"),0,0, SDT_INT, 0, AS_HAS_DEFAULT_VALUE },
	{ _WT("单步函数"), _WT("无返回值 (整数型 sqlite3上下文，整数型 参数个数，整数型 参数指针)。 "),0,0, _SDT_ALL, 0, NULL },
	{ _WT("完成函数"), _WT("无返回值 (整数型 sqlite3上下文)。 "),0,0, _SDT_ALL, 0, NULL },
	{ _WT("析构函数"), _WT("无返回值 (整数型 用户数据)，当数据库关闭或函数被覆盖时调用此方法。"),0,0, _SDT_ALL, 0, AS_DEFAULT_VALUE_IS_EMPTY },
};

ARG_INFO s_arg_context[] =
{
	{ _WT("Sqlite上下文"), _WT(""),0,0, SDT_INT, 0, NULL },
};

ARG_INFO s_arg_value[] =
{
	{ _WT("参数指针"), _WT(""),0,0, SDT_INT, 0, NULL },
	{ _WT("索引"), _WT("索引从0开始"),0,0, SDT_INT, 0, NULL },
};

ARG_INFO s_arg_result_text[] =
{
	{ _WT("Sqlite上下文"), _WT(""),0,0, SDT_INT, 0, NULL },
	{ _WT("文本"), _WT("用作返回的文本"),0,0, SDT_TEXT, 0, NULL },
	{ _WT("静态"), _WT(""),0,0, SDT_BOOL, 0, AS_HAS_DEFAULT_VALUE },
};
ARG_INFO s_arg_result_blob[] =
{
	{ _WT("Sqlite上下文"), _WT(""),0,0, SDT_INT, 0, NULL },
	{ _WT("字节集"), _WT("用作返回的字节集"),0,0, SDT_BIN, 0, NULL },
	{ _WT("静态"), _WT(""),0,0, SDT_BOOL, 0, AS_HAS_DEFAULT_VALUE },
};
ARG_INFO s_arg_result_int[] =
{
	{ _WT("Sqlite上下文"), _WT(""),0,0, SDT_INT, 0, NULL },
	{ _WT("整数"), _WT("用作返回的整数"),0,0, SDT_INT, 0, NULL },
};
ARG_INFO s_arg_result_int64[] =
{
	{ _WT("Sqlite上下文"), _WT(""),0,0, SDT_INT, 0, NULL },
	{ _WT("长整数"), _WT("用作返回的长整数"),0,0, SDT_INT64, 0, NULL },
};
ARG_INFO s_arg_result_double[] =
{
	{ _WT("Sqlite上下文"), _WT(""),0,0, SDT_INT, 0, NULL },
	{ _WT("双精度"), _WT("用作返回的双精度小数"),0,0, SDT_DOUBLE, 0, NULL },
};
ARG_INFO s_arg_result_error[] =
{
	{ _WT("Sqlite上下文"), _WT(""),0,0, SDT_INT, 0, NULL },
	{ _WT("错误信息"), _WT("用作返回的错误信息"),0,0, SDT_TEXT, 0, NULL },
};
ARG_INFO s_arg_result_error_code[] =
{
	{ _WT("Sqlite上下文"), _WT(""),0,0, SDT_INT, 0, NULL },
	{ _WT("错误代码"), _WT("用作返回的错误代码，“SQLITE_”开头常量"),0,0, SDT_INT, 0, NULL },
};
ARG_INFO s_arg_col_name[] =
{
	{ _WT("字段索引"), _WT("索引从0开始"),0,0, SDT_INT, 0, NULL },
};
ARG_INFO s_arg_wx_get_config[] =
{
	{ _WT("配置名"), _WT("“wxSQLITE_Config_”开头常量"),0,0, SDT_TEXT, 0, NULL },
};
ARG_INFO s_arg_wx_get_config_cipher[] =
{
	{ _WT("加密名"), _WT("“wxSQLITE_Cipher_”开头常量"),0,0, SDT_TEXT, 0, NULL },
	{ _WT("配置名"), _WT("“wxSQLITE_Config_”开头常量"),0,0, SDT_TEXT, 0, NULL },
};
ARG_INFO s_arg_wx_set_config[] =
{
	{ _WT("配置名"), _WT("“wxSQLITE_Config_”开头常量"),0,0, SDT_TEXT, 0, NULL },
	{ _WT("值"), _WT(""),0,0, SDT_INT, 0, NULL },
};
ARG_INFO s_arg_wx_set_config_cipher[] =
{
	{ _WT("加密名"), _WT("“wxSQLITE_Cipher_”开头常量"),0,0, SDT_TEXT, 0, NULL },
	{ _WT("配置名"), _WT("“wxSQLITE_Config_”开头常量"),0,0, SDT_TEXT, 0, NULL },
	{ _WT("值"), _WT(""),0,0, SDT_INT, 0, NULL },
};

ARG_INFO s_arg_busy_timeout[] =
{
	{ _WT("等待毫秒"), _WT("如果小于或等于0，则取消繁忙处理程序"),0,0, SDT_INT, 0, NULL },
};

ARG_INFO s_arg_busy_handler[] =
{
	{ _WT("处理程序"), _WT("返回值 整数型（整数型 用户数据，整数型 处理次数）。只能提供 【子程序指针】 或 【整数型】 如果为0则取消处理程序"),0,0, _SDT_ALL, 0, NULL },
	{ _WT("用户数据"), _WT("传递给处理程序的参数"),0,0, SDT_INT, 0, AS_DEFAULT_VALUE_IS_EMPTY },
};

ARG_INFO s_arg_db_filename[] =
{
	{ _WT("数据库名"), _WT("留空默认为：main"),0,0, SDT_TEXT, 0, AS_DEFAULT_VALUE_IS_EMPTY },
};

ARG_INFO s_arg_mutex[] =
{
	{ _WT("互斥体句柄"), _WT("可由 数据库.取互斥体() 返回。"),0,0, SDT_INT, 0, NULL },
};

ARG_INFO s_arg_progress_handler[] =
{
	{ _WT("近似总数"), _WT("在回调函数的连续调用之间评估的虚拟机指令的近似数量。如果小于1，将禁用进度处理回调"),0,0, SDT_INT, 0, NULL },
	{ _WT("处理程序"), _WT("返回值 整数型（整数型 用户数据）。返回非0停止处理。只能提供 【子程序指针】 或 【整数型】"),0,0, _SDT_ALL, 0, NULL },
	{ _WT("用户数据"), _WT("传递给处理程序的参数"),0,0, SDT_INT, 0, AS_DEFAULT_VALUE_IS_EMPTY },
};

ARG_INFO s_arg_next_stmt[] =
{
	{ _WT("当前记录集"), _WT(""),0,0, DTP_SQLITE_STMT, 0, NULL },
};

ARG_INFO s_arg_create_collation[] =
{
	{ _WT("名称"), _WT(""),0,0, SDT_TEXT, 0, NULL },
	{ _WT("编码"), _WT("“SQLITE_编码_”开头常量。不同的编码对应参数 <比较函数> 的数据指针"),0,0, SDT_INT, 0, NULL },
	{ _WT("用户数据"), _WT("传递给 <比较函数> 的自定义参数。"),0,0, SDT_INT, 0, AS_HAS_DEFAULT_VALUE },
	{ _WT("比较函数"), _WT("返回值：整数型（整数型 用户数据，整数型 长度1，整数型 数据指针1，整数型 长度2，整数型 数据指针2）。"),0,0, _SDT_ALL, 0, NULL },
	{ _WT("析构函数"), _WT("返回值：无（整数型 用户数据）；当被覆盖或数据库关闭是触发的回调函数。"),0,0, _SDT_ALL, 0, AS_DEFAULT_VALUE_IS_EMPTY },
};

// 命令信息
static CMD_INFO s_CmdInfo[] =
{
	/* { 中文名称, 英文名称, 对象描述, 所属类别(-1是数据类型的方法), 命令状态(CT_), 返回类型(SDT_), 此值保留, 对象等级(LVL_), 图像索引, 图像数量, 参数个数, 参数信息 } */
	{
		/*ccname*/	_WT("S3初始化"),
		/*egname*/	_WT("sqlite3_initialize"),
		/*explain*/	_WT("初始化Sqlite库，返回“SQLITE_”开头常量。"),
		/*category*/0,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("S3卸载"),
		/*egname*/	_WT("sqlite3_shutdown"),
		/*explain*/	_WT("释放由 初始化S3() 分配的资源，返回“SQLITE_”开头常量。卸载之前必须关闭所有打开的数据库或其他资源。"),
		/*category*/0,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("S3配置"),
		/*egname*/	_WT("sqlite3_config"),
		/*explain*/	_WT("此方法用于更改一些全局配置，必须在 初始化S3() 之前调用。"),
		/*category*/0,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_config) / sizeof(s_arg_config[0]),
		/*arg lp*/	s_arg_config,
	},
	{
		/*ccname*/	_WT("S3取版本"),
		/*egname*/	_WT("sqlite3_libversion"),
		/*explain*/	_WT("返回版本的文本形式"),
		/*category*/0,
		/*state*/	0,
		/*ret*/		SDT_TEXT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("S3取版本号"),
		/*egname*/	_WT("sqlite3_libversion_number"),
		/*explain*/	_WT("返回版本的数字形式"),
		/*category*/0,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("S3取用户数据"),
		/*egname*/	_WT("sqlite3_user_data"),
		/*explain*/	_WT("返回创建函数时提供的用户数据，这个方法应该是在创建函数的回调函数中使用。"),
		/*category*/0,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_context) / sizeof(s_arg_context[0]),
		/*arg lp*/	s_arg_context,
	},
	{
		/*ccname*/	_WT("S3取参数整数"),
		/*egname*/	_WT("sqlite3_value_int"),
		/*explain*/	_WT("返回参数的整数值，这个方法应该是在创建函数的回调函数中使用。"),
		/*category*/0,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_value) / sizeof(s_arg_value[0]),
		/*arg lp*/	s_arg_value,
	},
	{
		/*ccname*/	_WT("S3取参数长整数"),
		/*egname*/	_WT("sqlite3_value_int64"),
		/*explain*/	_WT("返回参数的长整数值，这个方法应该是在创建函数的回调函数中使用。"),
		/*category*/0,
		/*state*/	0,
		/*ret*/		SDT_INT64,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_value) / sizeof(s_arg_value[0]),
		/*arg lp*/	s_arg_value,
	},
	{
		/*ccname*/	_WT("S3取参数双精度"),
		/*egname*/	_WT("sqlite3_value_double"),
		/*explain*/	_WT("返回参数的双精度小数值，这个方法应该是在创建函数的回调函数中使用。"),
		/*category*/0,
		/*state*/	0,
		/*ret*/		SDT_DOUBLE,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_value) / sizeof(s_arg_value[0]),
		/*arg lp*/	s_arg_value,
	},
	{
		/*ccname*/	_WT("S3取参数文本"),
		/*egname*/	_WT("sqlite3_value_text"),
		/*explain*/	_WT("返回参数的文本值，这个方法应该是在创建函数的回调函数中使用。"),
		/*category*/0,
		/*state*/	0,
		/*ret*/		SDT_TEXT,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_value) / sizeof(s_arg_value[0]),
		/*arg lp*/	s_arg_value,
	},
	{
		/*ccname*/	_WT("S3取参数字节集"),
		/*egname*/	_WT("sqlite3_value_blob"),
		/*explain*/	_WT("返回参数的字节集值，这个方法应该是在创建函数的回调函数中使用。"),
		/*category*/0,
		/*state*/	0,
		/*ret*/		SDT_BIN,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_value) / sizeof(s_arg_value[0]),
		/*arg lp*/	s_arg_value,
	},
	{
		/*ccname*/	_WT("S3取参数类型"),
		/*egname*/	_WT("sqlite3_value_type"),
		/*explain*/	_WT("返回参数的类型，“SQLITE_类型_”开头常量，这个方法应该是在创建函数的回调函数中使用。"),
		/*category*/0,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_value) / sizeof(s_arg_value[0]),
		/*arg lp*/	s_arg_value,
	},
	{
		/*ccname*/	_WT("S3返回文本"),
		/*egname*/	_WT("sqlite3_result_text"),
		/*explain*/	_WT("指定创建自定义函数的返回值，这个方法应该是在创建函数的回调函数中使用。"),
		/*category*/0,
		/*state*/	0,
		/*ret*/		_SDT_NULL,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_result_text) / sizeof(s_arg_result_text[0]),
		/*arg lp*/	s_arg_result_text,
	},
	{
		/*ccname*/	_WT("S3返回字节集"),
		/*egname*/	_WT("sqlite3_result_blob"),
		/*explain*/	_WT("指定创建自定义函数的返回值，这个方法应该是在创建函数的回调函数中使用。"),
		/*category*/0,
		/*state*/	0,
		/*ret*/		_SDT_NULL,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_result_blob) / sizeof(s_arg_result_blob[0]),
		/*arg lp*/	s_arg_result_blob,
	},
	{
		/*ccname*/	_WT("S3返回整数"),
		/*egname*/	_WT("sqlite3_result_int"),
		/*explain*/	_WT("指定创建自定义函数的返回值，这个方法应该是在创建函数的回调函数中使用。"),
		/*category*/0,
		/*state*/	0,
		/*ret*/		_SDT_NULL,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_result_int) / sizeof(s_arg_result_int[0]),
		/*arg lp*/	s_arg_result_int,
	},
	{
		/*ccname*/	_WT("S3返回长整数"),
		/*egname*/	_WT("sqlite3_result_int64"),
		/*explain*/	_WT("指定创建自定义函数的返回值，这个方法应该是在创建函数的回调函数中使用。"),
		/*category*/0,
		/*state*/	0,
		/*ret*/		_SDT_NULL,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_result_int64) / sizeof(s_arg_result_int64[0]),
		/*arg lp*/	s_arg_result_int64,
	},
	{
		/*ccname*/	_WT("S3返回双精度"),
		/*egname*/	_WT("sqlite3_result_int64"),
		/*explain*/	_WT("指定创建自定义函数的返回值，这个方法应该是在创建函数的回调函数中使用。"),
		/*category*/0,
		/*state*/	0,
		/*ret*/		_SDT_NULL,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_result_double) / sizeof(s_arg_result_double[0]),
		/*arg lp*/	s_arg_result_double,
	},
	{
		/*ccname*/	_WT("S3返回空"),
		/*egname*/	_WT("sqlite3_result_null"),
		/*explain*/	_WT("指定创建自定义函数的返回值，这个方法应该是在创建函数的回调函数中使用。"),
		/*category*/0,
		/*state*/	0,
		/*ret*/		_SDT_NULL,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_context) / sizeof(s_arg_context[0]),
		/*arg lp*/	s_arg_context,
	},
	{
		/*ccname*/	_WT("S3返回错误信息"),
		/*egname*/	_WT("sqlite3_result_error"),
		/*explain*/	_WT("指定创建自定义函数的错误信息，这个方法应该是在创建函数的回调函数中使用。"),
		/*category*/0,
		/*state*/	0,
		/*ret*/		_SDT_NULL,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_result_error) / sizeof(s_arg_result_error[0]),
		/*arg lp*/	s_arg_result_error,
	},
	{
		/*ccname*/	_WT("S3返回错误代码"),
		/*egname*/	_WT("sqlite3_result_error_code"),
		/*explain*/	_WT("指定创建自定义函数的错误代码，这个方法应该是在创建函数的回调函数中使用。"),
		/*category*/0,
		/*state*/	0,
		/*ret*/		_SDT_NULL,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_result_error_code) / sizeof(s_arg_result_error_code[0]),
		/*arg lp*/	s_arg_result_error_code,
	},
	{
		/*ccname*/	_WT("S3取线程安全"),
		/*egname*/	_WT("sqlite3_threadsafe"),
		/*explain*/	_WT("仅返回编译时[SQLITE_THREADSAFE]宏的值，0表示单线程，1表示串行模式，2表示多线程。"),
		/*category*/0,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},

	{
		/*ccname*/	_WT("S3ToGB2312Ptr"),
		/*egname*/	_WT("Utf8PointToGB2312"),
		/*explain*/	_WT("将UTF8文本指针转换为GB2312文本。"),
		/*category*/0,
		/*state*/	0,
		/*ret*/		SDT_TEXT,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_to_gb2312) / sizeof(s_arg_to_gb2312[0]),
		/*arg lp*/	s_arg_to_gb2312,
	},
	{
		/*ccname*/	_WT("WXS3取配置"),
		/*egname*/	_WT("wxsqlite3_config"),
		/*explain*/	_WT("获取默认配置"),
		/*category*/0,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_wx_get_config) / sizeof(s_arg_wx_get_config[0]),
		/*arg lp*/	s_arg_wx_get_config,
	},
	{
		/*ccname*/	_WT("WXS3取配置加密"),
		/*egname*/	_WT("wxsqlite3_config_cipher"),
		/*explain*/	_WT("获取指定加密算法配置"),
		/*category*/0,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_wx_get_config_cipher) / sizeof(s_arg_wx_get_config_cipher[0]),
		/*arg lp*/	s_arg_wx_get_config_cipher,
	},
	{
		/*ccname*/	_WT("S3互斥体进入"),
		/*egname*/	_WT("sqlite3_mutex_enter"),
		/*explain*/	_WT("类似 进入许可区"),
		/*category*/0,
		/*state*/	0,
		/*ret*/		_SDT_NULL,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_mutex) / sizeof(s_arg_mutex[0]),
		/*arg lp*/	s_arg_mutex,
	},
	{
		/*ccname*/	_WT("S3互斥体退出"),
		/*egname*/	_WT("sqlite3_mutex_leave"),
		/*explain*/	_WT("类似 退出许可区"),
		/*category*/0,
		/*state*/	0,
		/*ret*/		_SDT_NULL,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_mutex) / sizeof(s_arg_mutex[0]),
		/*arg lp*/	s_arg_mutex,
	},
	//////////////////////////////////////////////////////////////////zySqlite数据库
	{
		/*ccname*/	_WT("关闭"),
		/*egname*/	_WT("sqlite3_close"),
		/*explain*/	_WT("关闭数据库的连接。如果有未完成的准备语句等，则会返回 #SQLITE_繁忙。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("关闭V2"),
		/*egname*/	_WT("sqlite3_close_v2"),
		/*explain*/	_WT("关闭数据库的连接。如果有未完成的准备语句等，会延迟释放相关资源。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("打开"),
		/*egname*/	_WT("sqlite3_open_v2"),
		/*explain*/	_WT("打开数据库。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_open_v2) / sizeof(s_arg_open_v2[0]),
		/*arg lp*/	s_arg_open_v2,
	},
	{
		/*ccname*/	_WT("密码"),
		/*egname*/	_WT("sqlite3_key"),
		/*explain*/	_WT("指定加密数据库密码，这个方法应该在 打开数据库 之后调用。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_key_v2) / sizeof(s_arg_key_v2[0]),
		/*arg lp*/	s_arg_key_v2,
	},
	{
		/*ccname*/	_WT("重置密码"),
		/*egname*/	_WT("sqlite3_rekey"),
		/*explain*/	_WT("如果之前不是加密数据库，则此方法为数据库增加密码，如果已经是加密数据库，则重新修改密码。这个方法应该在 打开数据库 之后调用。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_key_v2) / sizeof(s_arg_key_v2[0]),
		/*arg lp*/	s_arg_key_v2,
	},
	{
		/*ccname*/	_WT("执行SQL语句"),
		/*egname*/	_WT("sqlite3_exec"),
		/*explain*/	_WT("执行一条或多条SQL语句。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_exec) / sizeof(s_arg_exec[0]),
		/*arg lp*/	s_arg_exec,
	},
	{
		/*ccname*/	_WT("取错误代码"),
		/*egname*/	_WT("sqlite3_errcode"),
		/*explain*/	_WT("返回“SQLITE_”开头常量。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("取错误文本"),
		/*egname*/	_WT("sqlite3_errmsg"),
		/*explain*/	_WT(""),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_TEXT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("取句柄"),
		/*egname*/	_WT("getdb"),
		/*explain*/	_WT(""),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("置句柄"),
		/*egname*/	_WT("setdb"),
		/*explain*/	_WT("设置数据库句柄当当前类"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_set_hdb) / sizeof(s_arg_set_hdb[0]),
		/*arg lp*/	s_arg_set_hdb,
	},
	{
		/*ccname*/	_WT("取记录集"),
		/*egname*/	_WT("sqlite3_prepare_v2"),
		/*explain*/	_WT("准备一条记录集"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		DTP_SQLITE_STMT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_prepare) / sizeof(s_arg_prepare[0]),
		/*arg lp*/	s_arg_prepare,
	},
	{
		/*ccname*/	_WT("取最后插入ID"),
		/*egname*/	_WT("sqlite3_last_insert_rowid"),
		/*explain*/	_WT("每一个表都会有一个rowid列，如果已经有主键自增列，rowid则表示这个主键自增列的别名，这个方法返回最后插入的rowid"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT64,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("取影响行"),
		/*egname*/	_WT("sqlite3_changes"),
		/*explain*/	_WT("返回上一次insert、update、delete等操作的行数。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("开始事务"),
		/*egname*/	_WT("BeginTransaction"),
		/*explain*/	_WT("开始一个新的事务，如果提供<事务名称>参数则可以嵌套，但是不能使用<事务锁状态>参数。成功返回 #SQLITE_成功。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_begin_transaction) / sizeof(s_arg_begin_transaction[0]),
		/*arg lp*/	s_arg_begin_transaction,
	},
	{
		/*ccname*/	_WT("提交事务"),
		/*egname*/	_WT("CommitTransaction"),
		/*explain*/	_WT("成功返回 #SQLITE_成功。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_transaction) / sizeof(s_arg_transaction[0]),
		/*arg lp*/	s_arg_transaction,
	},
	{
		/*ccname*/	_WT("回滚事务"),
		/*egname*/	_WT("RollbackTransaction"),
		/*explain*/	_WT("成功返回 #SQLITE_成功。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_transaction) / sizeof(s_arg_transaction[0]),
		/*arg lp*/	s_arg_transaction,
	},
	{
		/*ccname*/	_WT("枚举表"),
		/*egname*/	_WT("EnumTable"),
		/*explain*/	_WT("返回记录集，字段0：表名称；字段1：创建时的SQL语句。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		DTP_SQLITE_STMT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("枚举索引"),
		/*egname*/	_WT("EnumIndex"),
		/*explain*/	_WT("返回记录集，字段0：索引名；字段1：创建时的SQL语句。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		DTP_SQLITE_STMT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_table_name) / sizeof(s_arg_table_name[0]),
		/*arg lp*/	s_arg_table_name,
	},
	{
		/*ccname*/	_WT("枚举视图"),
		/*egname*/	_WT("EnumView"),
		/*explain*/	_WT("返回记录集，字段0：视图名称；字段1：创建时的SQL语句。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		DTP_SQLITE_STMT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("枚举触发器"),
		/*egname*/	_WT("EnumTrigger"),
		/*explain*/	_WT("返回记录集，字段0：触发器名；字段1：创建时的SQL语句。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		DTP_SQLITE_STMT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_table_name) / sizeof(s_arg_table_name[0]),
		/*arg lp*/	s_arg_table_name,
	},
	{
		/*ccname*/	_WT("表是否存在"),
		/*egname*/	_WT("IsTableExist"),
		/*explain*/	_WT(""),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_table_name) / sizeof(s_arg_table_name[0]),
		/*arg lp*/	s_arg_table_name,
	}, 

	{
		/*ccname*/	_WT("取表内容"),
		/*egname*/	_WT("sqlite3_get_table"),
		/*explain*/	_WT("获取表的全部内容，返回一个二维文本数组"),
		/*category*/-1,
		/*state*/	CT_RETRUN_ARY_TYPE_DATA,
		/*ret*/		SDT_TEXT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_get_table) / sizeof(s_arg_get_table[0]),
		/*arg lp*/	s_arg_get_table,
	},
	{
		/*ccname*/	_WT("取表定义"),
		/*egname*/	_WT("GetTableSql"),
		/*explain*/	_WT("返回创建指定表时所用的SQL语句（CREATE TABLE ...）。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_TEXT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_table_name) / sizeof(s_arg_table_name[0]),
		/*arg lp*/	s_arg_table_name,
	},
	{
		/*ccname*/	_WT("创建表"),
		/*egname*/	_WT("CreateTable"),
		/*explain*/	_WT(""),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_create_table) / sizeof(s_arg_create_table[0]),
		/*arg lp*/	s_arg_create_table,
	},
	{
		/*ccname*/	_WT("删除表"),
		/*egname*/	_WT("DeleteTable"),
		/*explain*/	_WT(""),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_table_name) / sizeof(s_arg_table_name[0]),
		/*arg lp*/	s_arg_table_name,
	},
	{
		/*ccname*/	_WT("重命名表"),
		/*egname*/	_WT("ReNameTable"),
		/*explain*/	_WT(""),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_rename_table) / sizeof(s_arg_rename_table[0]),
		/*arg lp*/	s_arg_rename_table,
	},
	{
		/*ccname*/	_WT("添加字段"),
		/*egname*/	_WT("AddColumn"),
		/*explain*/	_WT(""),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_add_column) / sizeof(s_arg_add_column[0]),
		/*arg lp*/	s_arg_add_column,
	},
	{
		/*ccname*/	_WT("创建索引"),
		/*egname*/	_WT("CreateIndex"),
		/*explain*/	_WT(""),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_create_index) / sizeof(s_arg_create_index[0]),
		/*arg lp*/	s_arg_create_index,
	},
	{
		/*ccname*/	_WT("删除索引"),
		/*egname*/	_WT("DeleteIndex"),
		/*explain*/	_WT(""),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_delete_index) / sizeof(s_arg_delete_index[0]),
		/*arg lp*/	s_arg_delete_index,
	},
	{
		/*ccname*/	_WT("创建视图"),
		/*egname*/	_WT("CreateView"),
		/*explain*/	_WT(""),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_create_view) / sizeof(s_arg_create_view[0]),
		/*arg lp*/	s_arg_create_view,
	},
	{
		/*ccname*/	_WT("删除视图"),
		/*egname*/	_WT("DeleteView"),
		/*explain*/	_WT(""),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_delete_view) / sizeof(s_arg_delete_view[0]),
		/*arg lp*/	s_arg_delete_view,
	},
	{
		/*ccname*/	_WT("创建触发器"),
		/*egname*/	_WT("CreateTrigger"),
		/*explain*/	_WT(""),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_create_trigger) / sizeof(s_arg_create_trigger[0]),
		/*arg lp*/	s_arg_create_trigger,
	},
	{
		/*ccname*/	_WT("删除触发器"),
		/*egname*/	_WT("DeleteTrigger"),
		/*explain*/	_WT(""),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_delete_trigger) / sizeof(s_arg_delete_trigger[0]),
		/*arg lp*/	s_arg_delete_trigger,
	},
	{
		/*ccname*/	_WT("收缩数据库"),
		/*egname*/	_WT("ShrinkDB"),
		/*explain*/	_WT("收缩数据所占用的磁盘空间。经过大批量记录增删之后，数据库文件可能会变的较大，调用此方法可释放一部分缓冲区域，以减少文件尺寸。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("附加数据库"),
		/*egname*/	_WT("AttachDatabase"),
		/*explain*/	_WT("附加一个数据库到当前连接，使之可以同时操作多个数据库。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_attach_db) / sizeof(s_arg_attach_db[0]),
		/*arg lp*/	s_arg_attach_db,
	},
	{
		/*ccname*/	_WT("分离数据库"),
		/*egname*/	_WT("DetachDatabase"),
		/*explain*/	_WT("拆分一个之前使用“对象.附加数据库()”附加的数据库连接。此方法不能用于事务中"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_detach_db) / sizeof(s_arg_detach_db[0]),
		/*arg lp*/	s_arg_detach_db,
	},
	{
		/*ccname*/	_WT("备份数据库"),
		/*egname*/	_WT("BackupDatabase"),
		/*explain*/	_WT("备份指定的数据库到指定文件"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_backup_db) / sizeof(s_arg_backup_db[0]),
		/*arg lp*/	s_arg_backup_db,
	},

	{
		/*ccname*/	_WT("创建函数"),
		/*egname*/	_WT("CreateFunction"),
		/*explain*/	_WT("创建一个自定义Sql函数"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_create_function) / sizeof(s_arg_create_function[0]),
		/*arg lp*/	s_arg_create_function,
	},
	{
		/*ccname*/	_WT("创建聚合函数"),
		/*egname*/	_WT("CreateAggFunction"),
		/*explain*/	_WT("创建一个自定义Sql聚合函数，类似sum()"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_create_agg_function) / sizeof(s_arg_create_agg_function[0]),
		/*arg lp*/	s_arg_create_agg_function,
	},
	{
		/*ccname*/	_WT("繁忙超时"),
		/*egname*/	_WT("BusyTimeOut"),
		/*explain*/	_WT("当数据库处于繁忙状态时，可用此方法等待指定的毫秒数。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_busy_timeout) / sizeof(s_arg_busy_timeout[0]),
		/*arg lp*/	s_arg_busy_timeout,
	},
	{
		/*ccname*/	_WT("繁忙处理"),
		/*egname*/	_WT("BusyHandler"),
		/*explain*/	_WT("当数据库处于繁忙状态时，可用此方法等待，会调用回调函数，当回调函数返回0停止等待，返回非0则继续等待。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_busy_handler) / sizeof(s_arg_busy_handler[0]),
		/*arg lp*/	s_arg_busy_handler,
	},
	{
		/*ccname*/	_WT("取文件名"),
		/*egname*/	_WT("GetFileName"),
		/*explain*/	_WT("获取当前数据库的文件名。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_TEXT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_db_filename) / sizeof(s_arg_db_filename[0]),
		/*arg lp*/	s_arg_db_filename,
	},
	{
		/*ccname*/	_WT("是否只读"),
		/*egname*/	_WT("IsReadOnly"),
		/*explain*/	_WT("判断当前数据库是否只读。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_db_filename) / sizeof(s_arg_db_filename[0]),
		/*arg lp*/	s_arg_db_filename,
	},
	{
		/*ccname*/	_WT("取互斥体"),
		/*egname*/	_WT("GetDBMutex"),
		/*explain*/	_WT("仅当配置为 串行化 模式时，可返回数据库的互斥体。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	0,
	},
	{
		/*ccname*/	_WT("是否自动提交"),
		/*egname*/	_WT("IsAutoCommit"),
		/*explain*/	_WT("判断数据库是否处于自动提交模式。默认情况下，自动提交模式是打开的。 自动提交模式被开始事务禁用。 通过提交事务或回滚事务重新启用自动提交模式。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	0,
	},
	{
		/*ccname*/	_WT("进度处理"),
		/*egname*/	_WT("ProgressHandler"),
		/*explain*/	_WT("在 数据库.执行SQL语句()、数据库.取表内容()和 记录集.到下一行() 时，提供的进度处理回调函数，用于更新UI。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		_SDT_NULL,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_progress_handler) / sizeof(s_arg_progress_handler[0]),
		/*arg lp*/	s_arg_progress_handler,
	},
	{
		/*ccname*/	_WT("取下一记录集"),
		/*egname*/	_WT("NextStmt"),
		/*explain*/	_WT("获取与本数据库所关联的当前记录集的下一个记录集，可能返回空记录集。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		DTP_SQLITE_STMT,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_next_stmt) / sizeof(s_arg_next_stmt[0]),
		/*arg lp*/	s_arg_next_stmt,
	},
	{
		/*ccname*/	_WT("置配置"),
		/*egname*/	_WT("wxsqlite3_config"),
		/*explain*/	_WT("设置当前加密算法配置，必须在打开数据库后，设置密码前调用。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_wx_set_config) / sizeof(s_arg_wx_set_config[0]),
		/*arg lp*/	s_arg_wx_set_config,
	},
	{
		/*ccname*/	_WT("置配置加密"),
		/*egname*/	_WT("wxsqlite3_config_cipher"),
		/*explain*/	_WT("设置指定加密算法配置，必须在打开数据库后，设置密码前调用。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_wx_set_config_cipher) / sizeof(s_arg_wx_set_config_cipher[0]),
		/*arg lp*/	s_arg_wx_set_config_cipher,
	},
	///////////////////////////////////////////////记录集////////////////////////////////////////////////
	{
		/*ccname*/	_WT("析构函数"),
		/*egname*/	_WT("StmtDestruct"),
		/*explain*/	NULL,
		/*category*/-1,
		/*state*/	CT_IS_OBJ_FREE_CMD | CT_IS_HIDED,
		/*ret*/		_SDT_NULL,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("关闭"),
		/*egname*/	_WT("sqlite3_finalize"),
		/*explain*/	NULL,
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("置SQL语句"),
		/*egname*/	_WT("sqlite3_prepare_v2"),
		/*explain*/	_WT("设置本对象所使用的SQL语句。本方法等效于“zySqlite数据库.取记录集()”"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_set_sql) / sizeof(s_arg_set_sql[0]),
		/*arg lp*/	s_arg_set_sql,
	},
	{
		/*ccname*/	_WT("到下一行"),
		/*egname*/	_WT("sqlite3_step"),
		/*explain*/	_WT("移动到下一条记录"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("读字段文本值"),
		/*egname*/	_WT("sqlite3_column_text"),
		/*explain*/	_WT("读取当前记录中指定字段的值，并转换为文本型数据后返回。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_TEXT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_get_value) / sizeof(s_arg_get_value[0]),
		/*arg lp*/	s_arg_get_value,
	},
	{
		/*ccname*/	_WT("读字段整数值"),
		/*egname*/	_WT("sqlite3_column_int"),
		/*explain*/	_WT("读取当前记录中指定字段的整数型值。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_get_value) / sizeof(s_arg_get_value[0]),
		/*arg lp*/	s_arg_get_value,
	},
	{
		/*ccname*/	_WT("读字段双精度值"),
		/*egname*/	_WT("sqlite3_column_double"),
		/*explain*/	_WT("读取当前记录中指定字段的双精度小数型值。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_DOUBLE,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_get_value) / sizeof(s_arg_get_value[0]),
		/*arg lp*/	s_arg_get_value,
	},
	{
		/*ccname*/	_WT("读字段长整数值"),
		/*egname*/	_WT("sqlite3_column_int64"),
		/*explain*/	_WT("读取当前记录中指定字段的长整数型值。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT64,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_get_value) / sizeof(s_arg_get_value[0]),
		/*arg lp*/	s_arg_get_value,
	},
	{
		/*ccname*/	_WT("读字段字节集值"),
		/*egname*/	_WT("sqlite3_column_blob"),
		/*explain*/	_WT("读取当前记录中指定字段的字节集型值。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BIN,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_get_value) / sizeof(s_arg_get_value[0]),
		/*arg lp*/	s_arg_get_value,
	},
	{
		/*ccname*/	_WT("重置"),
		/*egname*/	_WT("sqlite3_reset"),
		/*explain*/	_WT("将记录集重置到第一条记录之前。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("取记录集列数"),
		/*egname*/	_WT("sqlite3_data_count"),
		/*explain*/	_WT("如果以移动到记录尾，将返回0。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("取字段数"),
		/*egname*/	_WT("sqlite3_column_count"),
		/*explain*/	_WT(""),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("取字段名"),
		/*egname*/	_WT("sqlite3_column_name"),
		/*explain*/	_WT(""),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_TEXT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_col_name) / sizeof(s_arg_col_name[0]),
		/*arg lp*/	s_arg_col_name,
	},
	{
		/*ccname*/	_WT("取字段表名"),
		/*egname*/	_WT("sqlite3_column_table_name"),
		/*explain*/	_WT(""),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_TEXT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_index) / sizeof(s_arg_index[0]),
		/*arg lp*/	s_arg_index,
	},
	{
		/*ccname*/	_WT("取字段数据库名"),
		/*egname*/	_WT("sqlite3_column_database_name"),
		/*explain*/	_WT(""),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_TEXT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_index) / sizeof(s_arg_index[0]),
		/*arg lp*/	s_arg_index,
	},
	{
		/*ccname*/	_WT("取字段长度"),
		/*egname*/	_WT("sqlite3_column_bytes"),
		/*explain*/	_WT("获取字段内容的字节长度。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_index) / sizeof(s_arg_index[0]),
		/*arg lp*/	s_arg_index,
	},
	{
		/*ccname*/	_WT("取字段定义类型"),
		/*egname*/	_WT("sqlite3_column_decltype"),
		/*explain*/	_WT("返回定义的字段类型名"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_TEXT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_index) / sizeof(s_arg_index[0]),
		/*arg lp*/	s_arg_index,
	},
	{
		/*ccname*/	_WT("取字段源名"),
		/*egname*/	_WT("sqlite3_column_origin_name"),
		/*explain*/	_WT(""),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_TEXT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_index) / sizeof(s_arg_index[0]),
		/*arg lp*/	s_arg_index,
	},
	{
		/*ccname*/	_WT("取字段类型"),
		/*egname*/	_WT("sqlite3_column_type"),
		/*explain*/	_WT("返回“SQLITE_类型_”开头常量。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_index) / sizeof(s_arg_index[0]),
		/*arg lp*/	s_arg_index,
	},
	{
		/*ccname*/	_WT("取SQL语句"),
		/*egname*/	_WT("sqlite3_sql"),
		/*explain*/	_WT("获取SQL语句。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_TEXT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("取展开SQL语句"),
		/*egname*/	_WT("sqlite3_expanded_sql"),
		/*explain*/	_WT("获取展开后的SQL语句。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_TEXT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("读字段值"),
		/*egname*/	_WT("GetFieldValue"),
		/*explain*/	_WT("读取当前记录中指定字段的值，并写入第二个参数“字段值”中。执行成功返回“真”，失败返回“假”。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_get_any_value) / sizeof(s_arg_get_any_value[0]),
		/*arg lp*/	s_arg_get_any_value,
	},
	{
		/*ccname*/	_WT("绑定参数"),
		/*egname*/	_WT("sqlite3_bind"),
		/*explain*/	_WT("应该先调用“zySqlite记录集.置SQL语句”或“zySqlite数据库.取记录集”，然后调用本方法来绑定SQL语句中的“?”、“?NNN”、“@VVV”、“:VVV”、“$VVV”值"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_bind) / sizeof(s_arg_bind[0]),
		/*arg lp*/	s_arg_bind,
	},
	{
		/*ccname*/	_WT("取参数个数"),
		/*egname*/	_WT("sqlite3_bind_parameter_count"),
		/*explain*/	_WT("获取一个SQL语句中的参数个数。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("清除绑定参数"),
		/*egname*/	_WT("sqlite3_clear_bindings"),
		/*explain*/	_WT("清除所有绑定的参数"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("取参数名"),
		/*egname*/	_WT("sqlite3_bind_parameter_name"),
		/*explain*/	_WT("根据参数索引获取参数名"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_TEXT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_bind_parameter_name) / sizeof(s_arg_bind_parameter_name[0]),
		/*arg lp*/	s_arg_bind_parameter_name,
	},
	{
		/*ccname*/	_WT("执行"),
		/*egname*/	_WT("Execute"),
		/*explain*/	_WT("执行准备好的记录集，如果是查询语句，返回 #SQLITE_行 表示还有下一行记录，返回 #SQLITE_完成 表示已经到了尾部，如果是非查询语句成功也也返回 #SQLITE_完成。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("查询标量文本"),
		/*egname*/	_WT("ExecuteScalar"),
		/*explain*/	_WT("执行查询语句，并返回第一行第一列的文本数据。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_TEXT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("查询标量"),
		/*egname*/	_WT("ExecuteScalar"),
		/*explain*/	_WT("执行查询语句，并返回第一行第一列的整数数据。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("取句柄"),
		/*egname*/	_WT("GethStmt"),
		/*explain*/	_WT(""),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("是否繁忙"),
		/*egname*/	_WT("IsBusy"),
		/*explain*/	_WT("记录集至少执行了一次 到下一行，且没有到末尾，又没有用 重置 命令，这时返回真。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("是否只读"),
		/*egname*/	_WT("IsReadOnly"),
		/*explain*/	_WT("如果记录集不会修改数据库的数据，则返回真。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	//////////////////////////////////////////////////////////////////////////新增
	{
		/*ccname*/	_WT("S3聚合上下文"),
		/*egname*/	_WT("AggregateContext"),
		/*explain*/	_WT("在同一聚合查询中，第一次调用会申请并清空内存返回，后续的调用会返回同一内存地址，sqlite3在聚合查询结束时会自动释放内存。"),
		/*category*/0,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("S3取数据库自上下文"),
		/*egname*/	_WT("ContextDBHandle"),
		/*explain*/	_WT("返回数据库句柄。"),
		/*category*/0,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("取总影响行数"),
		/*egname*/	_WT("GetTotalChanges"),
		/*explain*/	_WT("返回自打开数据库连接以来已完成的所有INSERT，UPDATE或DELETE语句插入，修改或删除的行总数，包括作为触发器程序一部分执行的行。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("取数据库句柄"),
		/*egname*/	_WT("GetDBHandle"),
		/*explain*/	_WT("获取与本记录集相关联的数据库句柄。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("取行数"),
		/*egname*/	_WT("GetLineCount"),
		/*explain*/	_WT("获取记录集行数。如果有绑定参数，必须绑定参数后才能获取到行数。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("创建排序规则"),
		/*egname*/	_WT("CreateCollation"),
		/*explain*/	_WT("创建一个自定义排序规则，同一个名称可以有多个编码的排序规则，之后查询排序可以用： ‘order by 字段名 collate 排序规则名’。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_create_collation) / sizeof(s_arg_create_collation[0]),
		/*arg lp*/	s_arg_create_collation,
	},
};
#endif

EXTERN_C void esqlite31_fn_sqlite3_initialize(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	pRetData->m_int = sqlite3_initialize();
}

EXTERN_C void esqlite31_fn_sqlite3_shutdown(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	pRetData->m_int = sqlite3_shutdown();
}

EXTERN_C void esqlite31_fn_sqlite3_config(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	if (pArgInf[1].m_dtDataType == _SDT_NULL)
	{
		pRetData->m_int = sqlite3_config(pArgInf[0].m_int);
	}
	else if (pArgInf[2].m_dtDataType == _SDT_NULL)
	{
		pRetData->m_int = sqlite3_config(pArgInf[0].m_int, pArgInf[1].m_int);
	}
	else
	{
		pRetData->m_int = sqlite3_config(pArgInf[0].m_int, pArgInf[1].m_int, pArgInf[2].m_int);
	}
	
}
EXTERN_C void esqlite31_fn_sqlite3_libversion(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	pRetData->m_pText = esqlite31_CloneTextData(sqlite3_libversion());
}
EXTERN_C void esqlite31_fn_sqlite3_libversion_number(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	pRetData->m_int = sqlite3_libversion_number();
}
EXTERN_C void esqlite31_fn_sqlite3_user_data(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	sqlite3_user_data_struct* pData = (sqlite3_user_data_struct*)sqlite3_user_data((sqlite3_context*)pArgInf[0].m_int);
	if (pData)
	{
		pRetData->m_int = (INT)pData->m_userData;
	}
}
EXTERN_C void esqlite31_fn_sqlite3_value_int(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	sqlite3_value** pValue = (sqlite3_value**)pArgInf[0].m_int;
	pRetData->m_int = sqlite3_value_int(pValue[pArgInf[1].m_int]);
}
EXTERN_C void esqlite31_fn_sqlite3_value_int64(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	sqlite3_value** pValue = (sqlite3_value**)pArgInf[0].m_int;
	pRetData->m_int64 = sqlite3_value_int64(pValue[pArgInf[1].m_int]);
}
EXTERN_C void esqlite31_fn_sqlite3_value_double(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	sqlite3_value** pValue = (sqlite3_value**)pArgInf[0].m_int;
	pRetData->m_double = sqlite3_value_double(pValue[pArgInf[1].m_int]);
}
EXTERN_C void esqlite31_fn_sqlite3_value_text(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	sqlite3_value** pValue = (sqlite3_value**)pArgInf[0].m_int;
	const wchar_t* wstr = (const wchar_t*)sqlite3_value_text16(pValue[pArgInf[1].m_int]);
	std::string str = zyW2A(wstr);
	pRetData->m_pText = esqlite31_CloneTextData(str.c_str());

}
EXTERN_C void esqlite31_fn_sqlite3_value_blob(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	sqlite3_value** pValue = (sqlite3_value**)pArgInf[0].m_int;
	const BYTE* pByte = (const BYTE*)sqlite3_value_blob(pValue[pArgInf[1].m_int]);
	int len = sqlite3_value_bytes(pValue[pArgInf[1].m_int]);
	pRetData->m_pBin = esqlite31_CloneBinData((LPBYTE)pByte, len);
}

EXTERN_C void esqlite31_fn_sqlite3_value_type(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	sqlite3_value** pValue = (sqlite3_value**)pArgInf[0].m_int;
	pRetData->m_int = sqlite3_value_type(pValue[pArgInf[1].m_int]);
}


EXTERN_C void esqlite31_fn_sqlite3_result_text(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	sqlite3_destructor_type type = SQLITE_TRANSIENT;
	if (pArgInf[2].m_bool)
	{
		type = SQLITE_STATIC;
	}
	std::wstring str = zyA2W(pArgInf[1].m_pText);
	sqlite3_result_text16((sqlite3_context*)pArgInf[0].m_int, str.c_str(), -1, type);
}

EXTERN_C void esqlite31_fn_sqlite3_result_blob(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	sqlite3_destructor_type type = SQLITE_TRANSIENT;
	LPBYTE pData = pArgInf[1].m_pBin;
	void* ptr = pData + sizeof(INT) * 2;
	int len = *((INT*)(pData + sizeof(INT)));
	if (pArgInf[2].m_bool)
	{
		type = SQLITE_STATIC;
	}
	sqlite3_result_blob((sqlite3_context*)pArgInf[0].m_int, ptr, len, type);
}

EXTERN_C void esqlite31_fn_sqlite3_result_int(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	sqlite3_result_int((sqlite3_context*)pArgInf[0].m_int, pArgInf[1].m_int);
}

EXTERN_C void esqlite31_fn_sqlite3_result_int64(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	sqlite3_result_int64((sqlite3_context*)pArgInf[0].m_int, pArgInf[1].m_int64);
}
EXTERN_C void esqlite31_fn_sqlite3_result_double(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	sqlite3_result_double((sqlite3_context*)pArgInf[0].m_int, pArgInf[1].m_double);
}
EXTERN_C void esqlite31_fn_sqlite3_result_null(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	sqlite3_result_null((sqlite3_context*)pArgInf[0].m_int);
}
EXTERN_C void esqlite31_fn_sqlite3_result_error(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	std::wstring str = zyA2W(pArgInf[1].m_pText);
	sqlite3_result_error16((sqlite3_context*)pArgInf[0].m_int, str.c_str(), -1);
}
EXTERN_C void esqlite31_fn_sqlite3_result_error_code(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	sqlite3_result_error_code((sqlite3_context*)pArgInf[0].m_int, pArgInf[1].m_int);
}
EXTERN_C void esqlite31_fn_sqlite3_threadsafe(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	pRetData->m_int = sqlite3_threadsafe();
}

EXTERN_C void esqlite31_fn_sqlite3_to_gb2312(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	const char* pUtf8 = (const char*)pArgInf[0].m_int;
	if (pUtf8 == NULL)
	{
		return;
	}
	std::string str = zyCodeConvert(pUtf8, -1, 65001, 936);

	pRetData->m_pText = esqlite31_CloneTextData(str.c_str());
}
EXTERN_C void esqlite31_fn_wxsqlite3_config(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	pRetData->m_int = wxsqlite3_config(NULL, pArgInf[0].m_pText, -1);
}
EXTERN_C void esqlite31_fn_wxsqlite3_config_cipher(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	pRetData->m_int = wxsqlite3_config_cipher(NULL, pArgInf[0].m_pText, pArgInf[1].m_pText, -1);
}
EXTERN_C void esqlite31_fn_sqlite3_mutex_enter(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	sqlite3_mutex_enter((sqlite3_mutex*)pArgInf[0].m_int);
}
EXTERN_C void esqlite31_fn_sqlite3_mutex_leave(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	sqlite3_mutex_leave((sqlite3_mutex*)pArgInf[0].m_int);
}

//////////////////////////////////////////////////////////////////////////数据库//////////////////////////////////////////////////////////////////////////

EXTERN_C void esqlite31_fn_sqlite3_close(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	int ret = SQLITE_ERROR;
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		ret = sqlite3_close(hdb);
		if (ret == SQLITE_OK)
		{
			((sqlite3_db_struct*)pArgInf[0].m_int)->hSqlite = NULL;
		}
		
	}
	pRetData->m_bool = ret == SQLITE_OK;
}
EXTERN_C void esqlite31_fn_sqlite3_close_v2(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	int ret = SQLITE_ERROR;
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		ret = sqlite3_close_v2(hdb);
		if (ret == SQLITE_OK)
		{
			((sqlite3_db_struct*)pArgInf[0].m_int)->hSqlite = NULL;
		}
	}
	pRetData->m_bool = ret == SQLITE_OK;
}
EXTERN_C void esqlite31_fn_sqlite3_open_v2(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	int ret = SQLITE_ERROR;
	if (hdb)
	{
		sqlite3_close(hdb);
		((sqlite3_db_struct*)pArgInf[0].m_int)->hSqlite = NULL;
	}
	std::string fileName = zyAllToUTF8(&pArgInf[1]);
	int flags = pArgInf[2].m_int;
	ret = sqlite3_open_v2(fileName.c_str(), &hdb, flags, NULL);
	if (ret != SQLITE_OK)
	{
		return;
	}
	((sqlite3_db_struct*)pArgInf[0].m_int)->hSqlite = hdb;
	pRetData->m_bool = ret == SQLITE_OK;
}
EXTERN_C void esqlite31_fn_sqlite3_key_v2(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		void* key = NULL;
		int len = 0;
		if (pArgInf[1].m_dtDataType == SDT_TEXT)
		{
			key = pArgInf[1].m_pText;
			len = strlen(pArgInf[1].m_pText);
		} else if (pArgInf[1].m_dtDataType == SDT_BIN)
		{
			key = pArgInf[1].m_pBin + sizeof(INT) * 2;
			len = *(LPINT)(pArgInf[1].m_pBin + sizeof(INT));
		}
		if (pArgInf[2].m_dtDataType == _SDT_NULL)
		{
			pRetData->m_int = sqlite3_key(hdb, key, len);
		}
		else
		{
			pRetData->m_int = sqlite3_key_v2(hdb, zyCodeConvert(pArgInf[2].m_pText).c_str() , key, len);
		}
		
	}
}

EXTERN_C void esqlite31_fn_sqlite3_rekey_v2(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		void* key = NULL;
		int len = 0;
		if (pArgInf[1].m_dtDataType == SDT_TEXT)
		{
			key = pArgInf[1].m_pText;
			len = strlen(pArgInf[1].m_pText);
		}
		else if (pArgInf[1].m_dtDataType == SDT_BIN)
		{
			key = pArgInf[1].m_pBin + sizeof(INT) * 2;
			len = *(LPINT)(pArgInf[1].m_pBin + sizeof(INT));
		}

		if (pArgInf[2].m_dtDataType == _SDT_NULL)
		{
			pRetData->m_int = sqlite3_rekey(hdb, key, len);
		}
		else
		{
			pRetData->m_int = sqlite3_rekey_v2(hdb, zyCodeConvert(pArgInf[2].m_pText).c_str(), key, len);
		}

	}
}

EXTERN_C void esqlite31_fn_sqlite3_exec(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	int ret = SQLITE_ERROR;
	if (hdb)
	{
		std::string sql = zyAllToUTF8(&pArgInf[1]);
		char* errorStr = NULL;
		sqlite3_exec_callback_struct ecs;
		ecs.m_callback = NULL;
		if (pArgInf[2].m_dwSubCodeAdr)
		{
			ecs.m_callback = (sqlite3_exec_callback)pArgInf[2].m_dwSubCodeAdr;
			ecs.m_userData = (void*)pArgInf[3].m_int;
		}
		if (ecs.m_callback)
		{
			ret = sqlite3_exec(hdb, sql.c_str(), zySqlite3_exec_callback, &ecs, &errorStr);
		}
		else
		{
			ret = sqlite3_exec(hdb, sql.c_str(), NULL, NULL, &errorStr);
		}
		
		if (errorStr && pArgInf[4].m_dtDataType!= _SDT_NULL)
		{
			if (*pArgInf[4].m_ppText)
			{
				esqlite31_MFree(*pArgInf[4].m_ppText);
			}
			*pArgInf[4].m_ppText = zyCloneToGB2312(errorStr);
		}
		if (errorStr)
		{
			sqlite3_free(errorStr);
		}
	}
	pRetData->m_bool = ret == SQLITE_OK;
}


EXTERN_C void esqlite31_fn_sqlite3_errcode(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		pRetData->m_int = sqlite3_errcode(hdb);
	}
}

EXTERN_C void esqlite31_fn_sqlite3_errmsg(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		pRetData->m_pText = zyCloneToGB2312(sqlite3_errmsg(hdb));
	}
}

EXTERN_C void esqlite31_fn_get_db(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	pRetData->m_int = (INT)hdb;
}

EXTERN_C void esqlite31_fn_set_db(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	((sqlite3_db_struct*)pArgInf[0].m_int)->hSqlite = (sqlite3*)pArgInf[1].m_int;
}

EXTERN_C void esqlite31_fn_sqlite3_prepare_v2(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	int rc = SQLITE_ERROR;
	CFreqMem freqMem;
	if (hdb)
	{
		std::string sql = zyAllToUTF8(&pArgInf[1]);
		sqlite3_stmt* hStmt = NULL;
		rc = sqlite3_prepare_v2(hdb, sql.c_str(), -1, &hStmt, NULL);
		freqMem.AddDWord((DWORD)hStmt);
	}
	else
	{
		freqMem.AddDWord(0);
	}
	if (pArgInf[2].m_dtDataType != _SDT_NULL)
	{
		*pArgInf[2].m_pBool = rc == SQLITE_OK;
	}
	pRetData->m_pCompoundData = freqMem.GetPtr();
}


EXTERN_C void esqlite31_fn_sqlite3_last_insert_rowid(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		pRetData->m_int64 = sqlite3_last_insert_rowid(hdb);
	}

}

EXTERN_C void esqlite31_fn_sqlite3_changes(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		pRetData->m_int = sqlite3_changes(hdb);
	}
}

EXTERN_C void esqlite31_fn_sqlite3_begin_transaction(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		if (pArgInf[1].m_dtDataType == _SDT_NULL)
		{
			switch (pArgInf[2].m_int)
			{
			case 1:
				pRetData->m_int = sqlite3_exec(hdb, "BEGIN IMMEDIATE", NULL, NULL, NULL);
				break;
			case 2:
				pRetData->m_int = sqlite3_exec(hdb, "BEGIN EXCLUSIVE", NULL, NULL, NULL);
				break;
			default:
				pRetData->m_int = sqlite3_exec(hdb, "BEGIN", NULL, NULL, NULL);
				break;
			}
			
		}
		else
		{
			std::string sql = "SAVEPOINT ";
			sql.append(pArgInf[1].m_pText);
			pRetData->m_int = sqlite3_exec(hdb, sql.c_str(), NULL, NULL, NULL);
		}
		
	}
}

EXTERN_C void esqlite31_fn_sqlite3_commit_transaction(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		if (pArgInf[1].m_dtDataType == _SDT_NULL)
		{
			pRetData->m_int = sqlite3_exec(hdb, "COMMIT", NULL, NULL, NULL);
		}
		else
		{
			std::string sql = "RELEASE ";
			sql.append(pArgInf[1].m_pText);
			pRetData->m_int = sqlite3_exec(hdb, sql.c_str(), NULL, NULL, NULL);
		}

	}
}
EXTERN_C void esqlite31_fn_sqlite3_rollback_transaction(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		if (pArgInf[1].m_dtDataType == _SDT_NULL)
		{
			pRetData->m_int = sqlite3_exec(hdb, "ROLLBACK", NULL, NULL, NULL);
		}
		else
		{
			std::string sql = "ROLLBACK TO ";
			sql.append(pArgInf[1].m_pText);
			pRetData->m_int = sqlite3_exec(hdb, sql.c_str(), NULL, NULL, NULL);
		}

	}
}
EXTERN_C void esqlite31_fn_sqlite3_enum_table(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	CFreqMem freqMem;
	if (hdb)
	{
		const char* sql = "select tbl_name,sql from sqlite_master where type='table' and tbl_name!='sqlite_sequence'";
		sqlite3_stmt* hStmt = NULL;
		sqlite3_prepare_v2(hdb, sql, -1, &hStmt, NULL);
		freqMem.AddDWord((DWORD)hStmt);
	}
	else
	{
		freqMem.AddDWord(0);
	}
	pRetData->m_pCompoundData = freqMem.GetPtr();
}

EXTERN_C void esqlite31_fn_sqlite3_enum_index(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	CFreqMem freqMem;
	if (hdb)
	{
		std::string sql = "select name,sql from sqlite_master where type='index' and tbl_name=‘";
		sql.append(pArgInf[1].m_pText);
		sql.append("'");
		std::string utf8 = zyCodeConvert(sql.c_str());
		sqlite3_stmt* hStmt = NULL;
		sqlite3_prepare_v2(hdb, utf8.c_str(), -1, &hStmt, NULL);
		freqMem.AddDWord((DWORD)hStmt);
	}
	else
	{
		freqMem.AddDWord(0);
	}
	pRetData->m_pCompoundData = freqMem.GetPtr();
}

EXTERN_C void esqlite31_fn_sqlite3_enum_view(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	CFreqMem freqMem;
	if (hdb)
	{
		const char* sql = "select name,sql from sqlite_master where type='view'";
		sqlite3_stmt* hStmt = NULL;
		sqlite3_prepare_v2(hdb, sql, -1, &hStmt, NULL);
		freqMem.AddDWord((DWORD)hStmt);
	}
	else
	{
		freqMem.AddDWord(0);
	}
	pRetData->m_pCompoundData = freqMem.GetPtr();
}

EXTERN_C void esqlite31_fn_sqlite3_enum_trigger(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	CFreqMem freqMem;
	if (hdb)
	{
		std::string sql = "select name,sql from sqlite_master where type='trigger' and tbl_name='";
		sql.append(pArgInf[1].m_pText);
		sql.append("'");
		std::string utf8 = zyCodeConvert(sql.c_str());
		sqlite3_stmt* hStmt = NULL;
		sqlite3_prepare_v2(hdb, utf8.c_str(), -1, &hStmt, NULL);
		freqMem.AddDWord((DWORD)hStmt);
	}
	else
	{
		freqMem.AddDWord(0);
	}
	pRetData->m_pCompoundData = freqMem.GetPtr();
}
EXTERN_C void esqlite31_fn_sqlite3_is_table_exist(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	int ret = 0;
	if (hdb)
	{
		std::string sql = "select count(*) from sqlite_master where type='table' and tbl_name='";
		sql.append(pArgInf[1].m_pText);
		sql.append("'");
		std::string utf8 = zyCodeConvert(sql.c_str());

		sqlite3_stmt* hStmt = NULL;
		sqlite3_prepare_v2(hdb, utf8.c_str(), -1, &hStmt, NULL);
		if (hStmt)
		{
			if (sqlite3_step(hStmt))
			{
				ret = sqlite3_column_int(hStmt, 0);
			}
			sqlite3_finalize(hStmt);
		}

	}
	pRetData->m_bool = ret;
}
EXTERN_C void esqlite31_fn_sqlite3_get_table(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	char** pResult = NULL;
	int ret = SQLITE_ERROR;
	int rownum = 0, colnum = 0;
	if (hdb)
	{
		std::string sql = zyAllToUTF8(&pArgInf[1]);
		char* pErrmsg;
		ret = sqlite3_get_table(hdb, sql.c_str(), &pResult, &rownum, &colnum, &pErrmsg);
		if (ret != SQLITE_OK)
		{
			if (pArgInf[4].m_dtDataType != _SDT_NULL)
			{
				if (*pArgInf[4].m_ppText)
				{
					esqlite31_MFree(*pArgInf[4].m_ppText);
				}
				std::string str = zyCodeConvert(pErrmsg, -1, 65001, 936);
				*pArgInf[4].m_ppText = esqlite31_CloneTextData(str.c_str());
			}
		}
	}
	if (ret == SQLITE_OK)
	{
		rownum++;
	}
	if (pArgInf[2].m_dtDataType!= _SDT_NULL)
	{
		*pArgInf[2].m_pInt = rownum;
	}
	if (pArgInf[3].m_dtDataType != _SDT_NULL)
	{
		*pArgInf[3].m_pInt = colnum;
	}
	if (pArgInf[5].m_dtDataType != _SDT_NULL)
	{
		*pArgInf[5].m_pBool = ret == SQLITE_OK;
	}


	LPINT table = (LPINT)esqlite31_MMalloc(sizeof(INT)*(1 + 2) + sizeof(INT)* rownum * colnum);
	if (table)
	{
		table[0] = 2;
		table[1] = rownum;
		table[2] = colnum;
	}

	if (pResult)
	{
		for (int i = 0; i < rownum; ++i)
		{
			for (int j = 0; j < colnum; ++j)
			{
				std::string str = zyCodeConvert(pResult[i*colnum + j], -1, 65001, 936);
				table[3 + i * colnum + j] = (INT)esqlite31_CloneTextData(str.c_str());
			}
		}
	}


	pRetData->m_pAryData = (void*)table;
	if (pResult)
	{
		sqlite3_free_table(pResult);
	}
	
}

EXTERN_C void esqlite31_fn_sqlite3_get_sql(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		std::string sql = "select sql from sqlite_master where type='table' and tbl_name='";
		sql.append(pArgInf[1].m_pText);
		sql.append("'");
		std::string utf8 = zyCodeConvert(sql.c_str());
		sqlite3_stmt* hStmt = NULL;
		if (sqlite3_prepare_v2(hdb, utf8.c_str(), -1, &hStmt, NULL) == SQLITE_OK)
		{
			if (sqlite3_step(hStmt) == SQLITE_ROW)
			{
				const wchar_t* wStr = (const wchar_t*)sqlite3_column_text16(hStmt, 0);
				pRetData->m_pText = esqlite31_CloneTextData(zyW2A(wStr).c_str());
			}
			sqlite3_finalize(hStmt);
		}
	}

}
EXTERN_C void esqlite31_fn_sqlite3_create_table(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		std::stringstream ss;
		ss << "create table if not exists " << pArgInf[1].m_pText;
		int fieldCount = *(((LPBYTE)pArgInf[2].m_pAryData) + sizeof(INT));
		LPINT pData = (LPINT)pArgInf[2].m_pAryData;
		pData += 2;
		if (fieldCount!=0)
		{
			ss << " (";
		}
		for (int i=0; i<fieldCount;++i)
		{
			sqlite3_field_info* pInfo = (sqlite3_field_info*)pData[i];

			ss << pInfo->name << " " << zyFieldTypeToString(pInfo->type);
			if (pInfo->len!=0)
			{
				ss << "(" << pInfo->len << ")";
			}
			if (pInfo->flags != 0)
			{
				ss << " " << zyFieldFlagsToString(pInfo->flags);
			}
			if (pInfo->defaultValue && *pInfo->defaultValue!='\0')
			{
				ss << " Default " << pInfo->defaultValue;
			}
			if (pInfo->check && *pInfo->check != '\0')
			{
				ss << " Check(" << pInfo->check << ")";
			}
			if (i < fieldCount -1)
			{
				ss << ",";
			}
		}
		if (pArgInf[3].m_dtDataType!=_SDT_NULL && *pArgInf[3].m_pText!='\0')
		{
			ss << ",Unique(" << pArgInf[3].m_pText << ")";
		}
		if (pArgInf[4].m_dtDataType != _SDT_NULL && *pArgInf[4].m_pText != '\0')
		{
			ss << ",primary key(" << pArgInf[4].m_pText << ")";
		}
		if (fieldCount != 0)
		{
			ss << ")";
		}
		std::string str = ss.str();
		std::string sql = zyCodeConvert(str.c_str());

		pRetData->m_bool = sqlite3_exec(hdb, sql.c_str(), NULL, NULL, NULL) == SQLITE_OK;
		
		
	}

}

EXTERN_C void esqlite31_fn_sqlite3_delete_table(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		std::string sql = "drop table if exists ";
		sql += pArgInf[1].m_pText;
		std::string utf8 = zyCodeConvert(sql.c_str());
		pRetData->m_bool = sqlite3_exec(hdb, utf8.c_str(), NULL, NULL, NULL) == SQLITE_OK;
	}
}

EXTERN_C void esqlite31_fn_sqlite3_rename_table(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		std::string sql = "alter table ";
		sql += pArgInf[1].m_pText;
		sql += " rename to ";
		sql += pArgInf[2].m_pText;
		std::string utf8 = zyCodeConvert(sql.c_str());
		pRetData->m_bool = sqlite3_exec(hdb, utf8.c_str(), NULL, NULL, NULL) == SQLITE_OK;
	}
}

EXTERN_C void esqlite31_fn_sqlite3_add_column(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		sqlite3_field_info* pInfo = (sqlite3_field_info*)pArgInf[2].m_pCompoundData;
		std::stringstream ss;
		ss << "alter table " << pArgInf[1].m_pText << " add column " << pInfo->name << " " << zyFieldTypeToString(pInfo->type);
		if (pInfo->len != 0)
		{
			ss << "(" << pInfo->len << ")";
		}
		if (pInfo->flags != 0)
		{
			ss << " " << zyFieldFlagsToString(pInfo->flags);
		}
		if (pInfo->defaultValue && *pInfo->defaultValue != '\0')
		{
			ss << " Default " << pInfo->defaultValue;
		}
		if (pInfo->check && *pInfo->check != '\0')
		{
			ss << " Check(" << pInfo->check << ")";
		}
		std::string utf8 = zyCodeConvert(ss.str().c_str());
		pRetData->m_bool = sqlite3_exec(hdb, utf8.c_str(), NULL, NULL, NULL) == SQLITE_OK;
	}
}

EXTERN_C void esqlite31_fn_sqlite3_create_index(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		std::stringstream ss;
		ss << "create " ;
		if (pArgInf[4].m_bool)
		{
			ss << "unique ";
		}
		ss << "index if not exists " << pArgInf[1].m_pText << " on " << pArgInf[2].m_pText << "(" << pArgInf[3].m_pText << ")";
		std::string utf8 = zyCodeConvert(ss.str().c_str());
		pRetData->m_bool = sqlite3_exec(hdb, utf8.c_str(), NULL, NULL, NULL) == SQLITE_OK;
	}
}
EXTERN_C void esqlite31_fn_sqlite3_delete_index(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		std::string str = "drop index if exists ";
		str += pArgInf[1].m_pText;
		
		std::string utf8 = zyCodeConvert(str.c_str());
		pRetData->m_bool = sqlite3_exec(hdb, utf8.c_str(), NULL, NULL, NULL) == SQLITE_OK;
	}
}

EXTERN_C void esqlite31_fn_sqlite3_create_view(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		std::stringstream ss;
		ss << "CREATE ";
		if (pArgInf[3].m_bool)
		{
			ss << "TEMPORARY ";
		}
		ss << "VIEW " << pArgInf[1].m_pText << " AS " << pArgInf[2].m_pText;
		std::string utf8 = zyCodeConvert(ss.str().c_str());
		pRetData->m_bool = sqlite3_exec(hdb, utf8.c_str(), NULL, NULL, NULL) == SQLITE_OK;
	}
}
EXTERN_C void esqlite31_fn_sqlite3_delete_view(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		std::string str = "DROP VIEW ";
		str += pArgInf[1].m_pText;

		std::string utf8 = zyCodeConvert(str.c_str());
		pRetData->m_bool = sqlite3_exec(hdb, utf8.c_str(), NULL, NULL, NULL) == SQLITE_OK;
	}
}


EXTERN_C void esqlite31_fn_sqlite3_create_trigger(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		std::stringstream ss;
		ss << "CREATE ";
		if (pArgInf[7].m_bool)
		{
			ss << "TEMPORARY ";
		}
		ss << "TRIGGER " << pArgInf[1].m_pText << " " ;
		if (pArgInf[6].m_int == 1)
		{
			ss << "BEFORE ";
		}
		else if (pArgInf[6].m_int == 2)
		{
			ss << "AFTER ";
		}

		switch (pArgInf[4].m_int)
		{
		case 0:
			ss << "INSERT ";
			break;
		case 1:
			ss << "DELETE ";
			break;
		case 2:
			ss << "UPDATE ";
			break;
		case 3:
			ss << "UPDATE OF "<< pArgInf[5].m_pText << " ";
			break;
		}

		ss << "ON " << pArgInf[2].m_pText << " BEGIN " << pArgInf[3].m_pText << " END";

		std::string utf8 = zyCodeConvert(ss.str().c_str());
		pRetData->m_bool = sqlite3_exec(hdb, utf8.c_str(), NULL, NULL, NULL) == SQLITE_OK;
	}
}

EXTERN_C void esqlite31_fn_sqlite3_delete_trigger(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		std::string str = "DROP TRIGGER ";
		str += pArgInf[1].m_pText;

		std::string utf8 = zyCodeConvert(str.c_str());
		pRetData->m_bool = sqlite3_exec(hdb, utf8.c_str(), NULL, NULL, NULL) == SQLITE_OK;
	}
}
EXTERN_C void esqlite31_fn_sqlite3_vacuum(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		pRetData->m_bool = sqlite3_exec(hdb, "VACUUM", NULL, NULL, NULL) == SQLITE_OK;
	}
}
EXTERN_C void esqlite31_fn_sqlite3_attach_database(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		std::string sql = "Attach Database '";
		sql += zyAllToUTF8(&pArgInf[1]);
		sql += "' as ";
		sql += zyCodeConvert(pArgInf[2].m_pText);

		if (pArgInf[3].m_dtDataType != _SDT_NULL)
		{
			sql += " KEY '" + zyCodeConvert(pArgInf[3].m_pText) + "'";
		}

		pRetData->m_bool = sqlite3_exec(hdb, sql.c_str(), NULL, NULL, NULL) == SQLITE_OK;
	}
}
EXTERN_C void esqlite31_fn_sqlite3_detach_database(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		std::string sql = "Detach Database ";
		sql += pArgInf[1].m_pText;
		std::string utf8 = zyCodeConvert(sql.c_str());
		pRetData->m_bool = sqlite3_exec(hdb, utf8.c_str(), NULL, NULL, NULL) == SQLITE_OK;
	}
}
EXTERN_C void esqlite31_fn_sqlite3_backup_database(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		sqlite3* pDb = ((sqlite3_db_struct*)pArgInf[1].m_pCompoundData)->hSqlite;
		int rc = SQLITE_ERROR;
		if (pDb == NULL)
		{
			return;
		}
		std::string dbNameDes = "main";
		std::string dbNameSrc = "main";
		if (pArgInf[2].m_dtDataType != _SDT_NULL)
		{
			dbNameDes = zyCodeConvert(pArgInf[2].m_pText);
		}
		if (pArgInf[3].m_dtDataType != _SDT_NULL)
		{
			dbNameSrc = zyCodeConvert(pArgInf[3].m_pText);
		}

		sqlite3_backup* pBackup = sqlite3_backup_init(pDb, dbNameDes.c_str(), hdb, dbNameSrc.c_str());
		if (!pBackup)
		{
			return;
		}

		sqlite3_backup_callback pCallback = (sqlite3_backup_callback)pArgInf[4].m_dwSubCodeAdr;

		do
		{
			rc = sqlite3_backup_step(pBackup, 5);
			if (pCallback)
			{
				int remaining = sqlite3_backup_remaining(pBackup);
				int count = sqlite3_backup_pagecount(pBackup);
				pCallback(remaining, count);
			}
			if (rc == SQLITE_BUSY || rc == SQLITE_LOCKED)
			{
				sqlite3_sleep(250);
			}

		} while (rc == SQLITE_OK || rc == SQLITE_BUSY || rc == SQLITE_LOCKED);

		rc = sqlite3_backup_finish(pBackup);
		pRetData->m_bool = rc == SQLITE_OK;

	}
}



EXTERN_C void esqlite31_fn_sqlite3_create_function(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	int rc = SQLITE_ERROR;
	if (hdb)
	{
		std::string name = zyCodeConvert(pArgInf[1].m_pText);
		sqlite3_user_data_struct* pData = new sqlite3_user_data_struct();
		if (!pData)
		{
			pRetData->m_bool = FALSE;
			return;
		}
		ZeroMemory(pData, sizeof(sqlite3_user_data_struct));

		pData->m_xFunc = (pxFunc)pArgInf[4].m_dwSubCodeAdr;
		if (pArgInf[5].m_dtDataType != _SDT_NULL)
		{
			pData->m_xDestroy = (pxDestroy)pArgInf[5].m_dwSubCodeAdr;
		}
		pData->m_userData = (void*)pArgInf[3].m_int;
		rc = sqlite3_create_function_v2(hdb, name.c_str(), pArgInf[2].m_int, SQLITE_UTF8, pData, zySqlite3_xFunc, NULL, NULL, zySqlite3_xDestroy);
	}
	pRetData->m_bool = rc == SQLITE_OK;
}

EXTERN_C void esqlite31_fn_sqlite3_create_agg_function(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	int rc = SQLITE_ERROR;
	if (hdb)
	{
		std::string name = zyCodeConvert(pArgInf[1].m_pText);

		sqlite3_user_data_struct* pData = new sqlite3_user_data_struct();
		if (!pData)
		{
			pRetData->m_bool = FALSE;
			return;
		}
		if (pArgInf[4].m_dtDataType != _SDT_NULL)
		{
			pData->m_xStep = (pxStep)pArgInf[4].m_dwSubCodeAdr;
		}
		if (pArgInf[5].m_dtDataType != _SDT_NULL)
		{
			pData->m_xFinal = (pxFinal)pArgInf[5].m_dwSubCodeAdr;
		}
		if (pArgInf[6].m_dtDataType != _SDT_NULL)
		{
			pData->m_xDestroy = (pxDestroy)pArgInf[6].m_dwSubCodeAdr;
		}
		pData->m_userData = (void*)pArgInf[3].m_int;

		rc = sqlite3_create_function_v2(hdb, name.c_str(), pArgInf[2].m_int, SQLITE_UTF8, pData, NULL, zySqlite3_xStep, zySqlite3_xFinal, zySqlite3_xDestroy);
	}
	pRetData->m_bool = rc == SQLITE_OK;
}

EXTERN_C void esqlite31_fn_sqlite3_busy_timeout(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		pRetData->m_int = sqlite3_busy_timeout(hdb, pArgInf[1].m_int);
	}
	else
	{
		pRetData->m_int = SQLITE_ERROR;
	}
}

EXTERN_C void esqlite31_fn_sqlite3_busy_handler(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		if (pArgInf[1].m_dwSubCodeAdr == NULL)
		{
			pRetData->m_int = sqlite3_busy_handler(hdb, NULL, NULL);
		}
		else
		{
			pRetData->m_int = sqlite3_busy_handler(hdb, zySqlite3_busy_handler_callback, (void*)pArgInf[1].m_dwSubCodeAdr);
		}

	}
	else
	{
		pRetData->m_int = SQLITE_ERROR;
	}
}

EXTERN_C void esqlite31_fn_sqlite3_db_filename(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		const char* dbName = "main";
		if (pArgInf[1].m_dtDataType != _SDT_NULL)
		{
			dbName = pArgInf[1].m_pText;
		}
		const char* utf8 = sqlite3_db_filename(hdb, dbName);
		std::string retStr = zyCodeConvert(utf8, -1, 65001, 936);
		pRetData->m_pText = esqlite31_CloneTextData(retStr.c_str());
	}
}
EXTERN_C void esqlite31_fn_sqlite3_db_readonly(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		const char* dbName = "main";
		if (pArgInf[1].m_dtDataType != _SDT_NULL)
		{
			dbName = pArgInf[1].m_pText;
		}
		pRetData->m_bool = sqlite3_db_readonly(hdb, dbName) == 1;
	}
}
EXTERN_C void esqlite31_fn_sqlite3_db_mutex(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		pRetData->m_int = (INT)sqlite3_db_mutex(hdb);
	}
}
EXTERN_C void esqlite31_fn_sqlite3_get_autocommit(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		pRetData->m_bool = sqlite3_get_autocommit(hdb);
	}
}
EXTERN_C void esqlite31_fn_sqlite3_progress_handler(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		if (pArgInf[2].m_pdwSubCodeAdr == NULL)
		{
			sqlite3_progress_handler(hdb, pArgInf[1].m_int, NULL, NULL);
		}
		else
		{
			sqlite3_progress_handler(hdb, pArgInf[1].m_int, zySqlite3_progress_handler_callback, (void*)pArgInf[2].m_pdwSubCodeAdr);
		}
		
	}
}
EXTERN_C void esqlite31_fn_sqlite3_next_stmt(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	CFreqMem freqMem;
	if (hdb)
	{
		sqlite3_stmt* pStmt = ((sqlite3_stmt_struct*)pArgInf[1].m_pCompoundData)->hStmt;
		sqlite3_stmt* hStmt = sqlite3_next_stmt(hdb, pStmt);
		freqMem.AddDWord((DWORD)hStmt);
	}
	else
	{
		freqMem.AddDWord(0);
	}
	pRetData->m_pCompoundData = freqMem.GetPtr();
}
EXTERN_C void esqlite31_fn_wxsqlite3_set_config(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		pRetData->m_int = wxsqlite3_config(hdb, pArgInf[1].m_pText, pArgInf[2].m_int);
	}
	else
	{
		pRetData->m_int = -1;
	}
}
EXTERN_C void esqlite31_fn_wxsqlite3_set_config_cipher(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		pRetData->m_int = wxsqlite3_config_cipher(hdb, pArgInf[1].m_pText, pArgInf[2].m_pText, pArgInf[3].m_int);
	}
	else
	{
		pRetData->m_int = -1;
	}
}
//////////////////////////////////////////////////////////////////////////记录集//////////////////////////////////////////////////////////////////////////

EXTERN_C void esqlite31_fn_stmt_destruct(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	/*SETUP_SQLITESTMT(pArgInf);
	if (hStmt)
	{
		sqlite3_finalize(hStmt);
		((sqlite3_stmt_struct*)pArgInf[0].m_pCompoundData)->hStmt = NULL;
	}*/
}

EXTERN_C void esqlite31_fn_stmt_close(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	int rc = SQLITE_ERROR;
	SETUP_SQLITESTMT(pArgInf);
	if (hStmt)
	{
		rc = sqlite3_finalize(hStmt);
		((sqlite3_stmt_struct*)pArgInf[0].m_pCompoundData)->hStmt = NULL;
	}
	pRetData->m_bool = rc == SQLITE_OK;
}

EXTERN_C void esqlite31_fn_stmt_set_sql(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	sqlite3* hdb = ((sqlite3_db_struct*)pArgInf[2].m_int)->hSqlite;
	if (!hdb)
	{
		pRetData->m_bool = FALSE;
		return;
	}
	SETUP_SQLITESTMT(pArgInf);
	if (hStmt)
	{
		sqlite3_finalize(hStmt);
		((sqlite3_stmt_struct*)pArgInf[0].m_int)->hStmt = NULL;
	}
	std::string utf8 = zyAllToUTF8(&pArgInf[1]);
	
	int rc = sqlite3_prepare_v2(hdb, utf8.c_str(), -1, &hStmt, NULL);
	((sqlite3_stmt_struct*)pArgInf[0].m_int)->hStmt = hStmt;
	pRetData->m_bool = rc == SQLITE_OK;
}

EXTERN_C void esqlite31_fn_stmt_next(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITESTMT(pArgInf);
	if (hStmt)
	{
		pRetData->m_bool = sqlite3_step(hStmt) == SQLITE_ROW;
	}
}

EXTERN_C void esqlite31_fn_stmt_get_text_value(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITESTMT(pArgInf);
	if (hStmt)
	{
		int index = zyGetColumnIndex(hStmt, &pArgInf[1]);
		if (index == -1 && pArgInf[2].m_dtDataType != _SDT_NULL)
		{
			*pArgInf[2].m_pBool = FALSE;
			return;
		}
		const void* wStr = sqlite3_column_text16(hStmt, index);
		std::string str = zyW2A((const wchar_t*)wStr);
		pRetData->m_pText = esqlite31_CloneTextData(str.c_str());
		if (pArgInf[2].m_dtDataType != _SDT_NULL)
		{
			*pArgInf[2].m_pBool = TRUE;
		}
	}
}

EXTERN_C void esqlite31_fn_stmt_get_int_value(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITESTMT(pArgInf);
	if (hStmt)
	{
		int index = zyGetColumnIndex(hStmt, &pArgInf[1]);
		if (index == -1 && pArgInf[2].m_dtDataType != _SDT_NULL)
		{
			*pArgInf[2].m_pBool = FALSE;
			return;
		}
		if (pArgInf[2].m_dtDataType != _SDT_NULL)
		{
			*pArgInf[2].m_pBool = TRUE;
		}
		pRetData->m_int = sqlite3_column_int(hStmt, index);
	}
}
EXTERN_C void esqlite31_fn_stmt_get_double_value(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITESTMT(pArgInf);
	if (hStmt)
	{
		int index = zyGetColumnIndex(hStmt, &pArgInf[1]);
		if (index == -1 && pArgInf[2].m_dtDataType != _SDT_NULL)
		{
			*pArgInf[2].m_pBool = FALSE;
			return;
		}
		if (pArgInf[2].m_dtDataType != _SDT_NULL)
		{
			*pArgInf[2].m_pBool = TRUE;
		}
		pRetData->m_double = sqlite3_column_double(hStmt, index);
	}
}
EXTERN_C void esqlite31_fn_stmt_get_int64_value(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITESTMT(pArgInf);
	if (hStmt)
	{
		int index = zyGetColumnIndex(hStmt, &pArgInf[1]);
		if (index == -1 && pArgInf[2].m_dtDataType != _SDT_NULL)
		{
			*pArgInf[2].m_pBool = FALSE;
			return;
		}
		if (pArgInf[2].m_dtDataType != _SDT_NULL)
		{
			*pArgInf[2].m_pBool = TRUE;
		}
		pRetData->m_int64 = sqlite3_column_int64(hStmt, index);
	}
}
EXTERN_C void esqlite31_fn_stmt_get_blob_value(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITESTMT(pArgInf);
	if (hStmt)
	{
		int index = zyGetColumnIndex(hStmt, &pArgInf[1]);
		if (index == -1 && pArgInf[2].m_dtDataType != _SDT_NULL)
		{
			*pArgInf[2].m_pBool = FALSE;
			return;
		}
		if (pArgInf[2].m_dtDataType != _SDT_NULL)
		{
			*pArgInf[2].m_pBool = TRUE;
		}
		int len = sqlite3_column_bytes(hStmt, index);
		LPBYTE ptr = (LPBYTE)sqlite3_column_blob(hStmt, index);
		pRetData->m_pBin = esqlite31_CloneBinData(ptr, len);
	}
}
EXTERN_C void esqlite31_fn_stmt_reset(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITESTMT(pArgInf);
	if (hStmt)
	{
		pRetData->m_int = sqlite3_reset(hStmt);
	}
}
EXTERN_C void esqlite31_fn_stmt_data_count(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITESTMT(pArgInf);
	if (hStmt)
	{
		pRetData->m_int = sqlite3_data_count(hStmt);
	}
}
EXTERN_C void esqlite31_fn_stmt_column_count(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITESTMT(pArgInf);
	if (hStmt)
	{
		pRetData->m_int = sqlite3_column_count(hStmt);
	}
}
EXTERN_C void esqlite31_fn_stmt_column_name(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITESTMT(pArgInf);
	if (hStmt)
	{
		int index = pArgInf[1].m_int;
		if (index < 0)
		{
			return;
		}
		std::string str = zyW2A( (const wchar_t*)sqlite3_column_name16(hStmt, index));
		pRetData->m_pText = esqlite31_CloneTextData(str.c_str());
	}
}
EXTERN_C void esqlite31_fn_stmt_column_table_name(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITESTMT(pArgInf);
	if (hStmt)
	{
		int index = zyGetColumnIndex(hStmt, &pArgInf[1]);
		if (index == -1)
		{
			return;
		}
		std::string str = zyW2A((const wchar_t*)sqlite3_column_table_name16(hStmt, index));
		pRetData->m_pText = esqlite31_CloneTextData(str.c_str());
	}
}
EXTERN_C void esqlite31_fn_stmt_column_database_name(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITESTMT(pArgInf);
	if (hStmt)
	{
		int index = zyGetColumnIndex(hStmt, &pArgInf[1]);
		if (index == -1)
		{
			return;
		}
		std::string str = zyW2A((const wchar_t*)sqlite3_column_database_name16(hStmt, index));
		pRetData->m_pText = esqlite31_CloneTextData(str.c_str());
	}
}
EXTERN_C void esqlite31_fn_stmt_column_bytes(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITESTMT(pArgInf);
	if (hStmt)
	{
		int index = zyGetColumnIndex(hStmt, &pArgInf[1]);
		if (index == -1)
		{
			return;
		}
		pRetData->m_int = sqlite3_column_bytes(hStmt, index);
	}
}
EXTERN_C void esqlite31_fn_stmt_column_decltype(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITESTMT(pArgInf);
	if (hStmt)
	{
		int index = zyGetColumnIndex(hStmt, &pArgInf[1]);
		if (index == -1)
		{
			return;
		}
		std::string str = zyW2A((const wchar_t*)sqlite3_column_decltype16(hStmt, index));
		pRetData->m_pText = esqlite31_CloneTextData(str.c_str());
	}
}
EXTERN_C void esqlite31_fn_stmt_column_origin_name(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITESTMT(pArgInf);
	if (hStmt)
	{
		int index = zyGetColumnIndex(hStmt, &pArgInf[1]);
		if (index == -1)
		{
			return;
		}
		std::string str = zyW2A((const wchar_t*)sqlite3_column_origin_name16(hStmt, index));
		pRetData->m_pText = esqlite31_CloneTextData(str.c_str());
	}
}
EXTERN_C void esqlite31_fn_stmt_column_type(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITESTMT(pArgInf);
	if (hStmt)
	{
		int index = zyGetColumnIndex(hStmt, &pArgInf[1]);
		if (index == -1)
		{
			return;
		}
		pRetData->m_int = sqlite3_column_type(hStmt, index);
	}
}

EXTERN_C void esqlite31_fn_stmt_sql(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITESTMT(pArgInf);
	if (hStmt)
	{
		const char* sql = sqlite3_sql(hStmt);
		if (sql)
		{
			std::string str = zyCodeConvert(sql, -1, 65001, 936);
			pRetData->m_pText = esqlite31_CloneTextData(str.c_str());
		}
	}
}

EXTERN_C void esqlite31_fn_stmt_expanded_sql(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITESTMT(pArgInf);
	if (hStmt)
	{
		char* sql = sqlite3_expanded_sql(hStmt);
		if (sql)
		{
			std::string str = zyCodeConvert(sql, -1, 65001, 936);
			pRetData->m_pText = esqlite31_CloneTextData(str.c_str());
			sqlite3_free(sql);
		}
		
	}
}
EXTERN_C void esqlite31_fn_stmt_get_any_value(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITESTMT(pArgInf);
	if (hStmt)
	{
		int index = zyGetColumnIndex(hStmt, &pArgInf[1]);
		if (index == -1)
		{
			pRetData->m_bool = FALSE;
			return;
		}
		switch (pArgInf[2].m_dtDataType)
		{
			case SDT_INT:
				*pArgInf[2].m_pInt = sqlite3_column_int(hStmt, index);
				pRetData->m_bool = TRUE;
				break;
			case SDT_BYTE:
				*pArgInf[2].m_pByte = (BYTE)sqlite3_column_int(hStmt, index);
				pRetData->m_bool = TRUE;
				break;
			case SDT_SHORT:
				*pArgInf[2].m_pShort = (short)sqlite3_column_int(hStmt, index);
				pRetData->m_bool = TRUE;
				break;
			case SDT_BOOL:
				*pArgInf[2].m_pBool = sqlite3_column_int(hStmt, index) != 0;
				pRetData->m_bool = TRUE;
				break;
			case SDT_FLOAT:
				*pArgInf[2].m_pFloat = (float)sqlite3_column_double(hStmt, index);
				pRetData->m_bool = TRUE;
				break;
			case SDT_DOUBLE:
				*pArgInf[2].m_pDouble = sqlite3_column_double(hStmt, index);
				pRetData->m_bool = TRUE;
				break;
			case SDT_INT64:
				*pArgInf[2].m_pInt64 = sqlite3_column_int64(hStmt, index);
				pRetData->m_bool = TRUE;
				break;
			case SDT_TEXT:
			{
				if (*pArgInf[2].m_ppText)
				{
					esqlite31_MFree(*pArgInf[2].m_ppText);
				}
				const wchar_t* text16 = (const wchar_t*)sqlite3_column_text16(hStmt, index);
				std::string str = zyW2A(text16);
				*pArgInf[2].m_ppText = esqlite31_CloneTextData(str.c_str());
				pRetData->m_bool = TRUE;
				break;
			}
			case SDT_BIN:
			{
				if (*pArgInf[2].m_ppBin)
				{
					esqlite31_MFree(*pArgInf[2].m_ppBin);
				}
				int len = sqlite3_column_bytes(hStmt, index);
				LPBYTE ptr = (LPBYTE)sqlite3_column_blob(hStmt, index);
				*pArgInf[2].m_ppBin = esqlite31_CloneBinData(ptr, len);
				pRetData->m_bool = TRUE;
				break;
			}
		}
	}
}

EXTERN_C void esqlite31_fn_stmt_bind(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITESTMT(pArgInf);
	int rc = SQLITE_OK;
	if (hStmt)
	{
		int index = 0;
		if (pArgInf[1].m_dtDataType == SDT_TEXT)
		{
			index = sqlite3_bind_parameter_index(hStmt, zyCodeConvert(pArgInf[1].m_pText).c_str());
		}
		else
		{
			index = pArgInf[1].m_int;
		}

		if (index <= 0)
		{
			pRetData->m_bool = FALSE;
			return;
		}

		switch (pArgInf[2].m_dtDataType)
		{
			case SDT_INT:
				rc = sqlite3_bind_int(hStmt, index, pArgInf[2].m_int);
				break;
			case SDT_BYTE:
				rc = sqlite3_bind_int(hStmt, index, pArgInf[2].m_byte);
				break;
			case SDT_SHORT:
				rc = sqlite3_bind_int(hStmt, index, pArgInf[2].m_short);
				break;
			case SDT_BOOL:
				rc = sqlite3_bind_int(hStmt, index, pArgInf[2].m_bool);
				break;
			case SDT_FLOAT:
				rc = sqlite3_bind_double(hStmt, index, pArgInf[2].m_float);
				break;
			case SDT_DOUBLE:
				rc = sqlite3_bind_double(hStmt, index, pArgInf[2].m_double);
				break;
			case SDT_INT64:
				rc = sqlite3_bind_int64(hStmt, index, pArgInf[2].m_int64);
				break;
			case SDT_TEXT:
			{
				std::string str = zyCodeConvert(pArgInf[2].m_pText);
				rc = sqlite3_bind_text(hStmt, index, str.c_str(), str.length(), SQLITE_TRANSIENT);
				break;
			}
			case SDT_BIN:
			{
				int len = *((LPINT)(pArgInf[2].m_pBin + sizeof(INT)));
				rc = sqlite3_bind_blob(hStmt, index, pArgInf[2].m_pBin + sizeof(INT)*2, len, SQLITE_TRANSIENT);
				break;
			}
			case _SDT_NULL:
				rc = sqlite3_bind_null(hStmt, index);
				break;
		}

		pRetData->m_bool = rc == SQLITE_OK;
	}
}

EXTERN_C void esqlite31_fn_stmt_bind_parameter_count(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITESTMT(pArgInf);
	if (hStmt)
	{
		pRetData->m_int = sqlite3_bind_parameter_count(hStmt);
	}
}
EXTERN_C void esqlite31_fn_stmt_clear_bindings(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITESTMT(pArgInf);
	if (hStmt)
	{
		pRetData->m_bool = sqlite3_clear_bindings(hStmt) == SQLITE_OK;
	}
}
EXTERN_C void esqlite31_fn_stmt_bind_parameter_name(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITESTMT(pArgInf);
	if (hStmt)
	{
		 const char* utf8 = sqlite3_bind_parameter_name(hStmt, pArgInf[1].m_int);
		 if (utf8)
		 {
			 std::string str = zyCodeConvert(utf8, -1, 65001, 936);
			 pRetData->m_pText = esqlite31_CloneTextData(str.c_str());
		 }
	}
}
EXTERN_C void esqlite31_fn_stmt_execute(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITESTMT(pArgInf);
	if (hStmt)
	{
		pRetData->m_int = sqlite3_step(hStmt);
	}
}
EXTERN_C void esqlite31_fn_stmt_execute_scalar_text(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITESTMT(pArgInf);
	if (hStmt)
	{
		int ret = sqlite3_step(hStmt);
		if (ret == SQLITE_ROW)
		{
			const wchar_t* text =(const wchar_t*)sqlite3_column_text16(hStmt, 0);
			std::string str = zyW2A(text);
			pRetData->m_pText = esqlite31_CloneTextData(str.c_str());
		}
	}
}
EXTERN_C void esqlite31_fn_stmt_execute_scalar(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITESTMT(pArgInf);
	if (hStmt)
	{
		int ret = sqlite3_step(hStmt);
		if (ret == SQLITE_ROW)
		{
			pRetData->m_int = sqlite3_column_int(hStmt, 0);
		}
	}
}
EXTERN_C void esqlite31_fn_stmt_get_handle(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITESTMT(pArgInf);
	pRetData->m_int = (INT)hStmt;
}

EXTERN_C void esqlite31_fn_stmt_busy(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITESTMT(pArgInf);
	pRetData->m_bool = sqlite3_stmt_busy(hStmt);
}
EXTERN_C void esqlite31_fn_stmt_readonly(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITESTMT(pArgInf);
	pRetData->m_bool = sqlite3_stmt_readonly(hStmt);
}
//////////////////////////////////////////////////////////////////////////新增//////////////////////////////////////////////////////////////////////////

EXTERN_C void esqlite31_fn_aggregate_context(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	pRetData->m_int = (INT)sqlite3_aggregate_context((sqlite3_context*)pArgInf[0].m_int, pArgInf[1].m_int);
}

EXTERN_C void esqlite31_fn_context_db_handle(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	pRetData->m_int = (INT)sqlite3_context_db_handle((sqlite3_context*)pArgInf[0].m_int);
}
EXTERN_C void esqlite31_fn_sqlite3_total_changes(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	if (hdb)
	{
		pRetData->m_int = sqlite3_total_changes(hdb);
	}

}

EXTERN_C void esqlite31_fn_stmt_db_handle(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITESTMT(pArgInf);
	if (hStmt)
	{
		pRetData->m_int = (INT)sqlite3_db_handle(hStmt);
	}

}
EXTERN_C void esqlite31_fn_stmt_get_line_count(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITESTMT(pArgInf);
	if (hStmt)
	{
		char* pStrUTF8 = sqlite3_expanded_sql(hStmt);
		if (!pStrUTF8)
		{
			return;
		}
		size_t len = strlen(pStrUTF8);
		char* strNewSql = (char*)malloc(len + 25);
		memset(strNewSql, 0, len + 25);
		strcpy(strNewSql, "select count(*) from (");
		strcat(strNewSql, pStrUTF8);
		strcat(strNewSql, ")");
		sqlite3_free(pStrUTF8);

		sqlite3* db = sqlite3_db_handle(hStmt);
		if (!db)
		{
			free(strNewSql);
			return;
		}
		sqlite3_stmt* stmt = NULL;
		if (sqlite3_prepare_v2(db, strNewSql, -1, &stmt, NULL) != SQLITE_OK)
		{
			free(strNewSql);
			return;
		}

		if (sqlite3_step(stmt) == SQLITE_ROW)
		{
			pRetData->m_int = sqlite3_column_int(stmt, 0);
		}

		sqlite3_finalize(stmt);
		free(strNewSql);
		return;


		/*char* pStrUTF8 = sqlite3_expanded_sql(hStmt);
		if (!pStrUTF8)
		{
			return;
		}
		if (strnicmp(pStrUTF8, "select", 6) != 0)
		{
			sqlite3_free(pStrUTF8);
			return;
		}

		char* ptr = pStrUTF8;
		int subcount = 0;
		bool hasFrom = false;
		int index = 0;

		char c = *ptr;
		while (c)
		{
			if (c == '(')
			{
				subcount++;
			}
			else if (c == ')')
			{
				subcount--;
			}
			else if (c == 'F' || c == 'f')
			{
				if (subcount == 0)
				{
					if (strnicmp(ptr, "from", 4) == 0)
					{
						hasFrom = true;
						break;
					}
				}
			}
			index++;
			c = *(++ptr);
		}

		if (hasFrom)
		{
			int len = strlen(pStrUTF8);
			char* newSql = (char*)malloc(len + 10);

			if (!newSql)
			{
				sqlite3_free(pStrUTF8);
				return;
			}
			memset(newSql, 0, len + 10);

			memcpy(newSql, "select count(*) ", 16);
			memcpy(newSql + 16, ptr, len - index);

			sqlite3_free(pStrUTF8);

			sqlite3* db = sqlite3_db_handle(hStmt);
			if (!db)
			{
				free(newSql);
				return;
			}
			sqlite3_stmt* stmt = NULL;
			if (sqlite3_prepare_v2(db, newSql, -1, &stmt, NULL) != SQLITE_OK)
			{
				free(newSql);
				return;
			}

			if (sqlite3_step(stmt) == SQLITE_ROW)
			{
				pRetData->m_int = sqlite3_column_int(stmt, 0);
			}

			sqlite3_finalize(stmt);
			free(newSql);
			return;
		}
		if (pStrUTF8)
		{
			sqlite3_free(pStrUTF8);
		}
		pRetData->m_int = 1;
		return;*/

	}

}

EXTERN_C void esqlite31_fn_sqlite3_create_collation(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_SQLITEDB(pArgInf);
	int rc = SQLITE_ERROR;
	if (hdb)
	{
		std::string name = zyCodeConvert(pArgInf[1].m_pText);
		sqlite3_user_data_struct* pData = new sqlite3_user_data_struct();
		if (!pData)
		{
			pRetData->m_bool = FALSE;
			return;
		}
		ZeroMemory(pData, sizeof(sqlite3_user_data_struct));

		pData->m_xCompare = (pxCompare)pArgInf[4].m_dwSubCodeAdr;
		if (pArgInf[5].m_dtDataType != _SDT_NULL)
		{
			pData->m_xDestroy = (pxDestroy)pArgInf[5].m_dwSubCodeAdr;
		}
		pData->m_userData = (void*)pArgInf[3].m_int;
		rc = sqlite3_create_collation_v2(hdb, name.c_str(), pArgInf[2].m_int, pData, zySqlite3_xCompare, zySqlite3_xDestroy);
	}
	pRetData->m_bool = rc == SQLITE_OK;
}

#ifndef __E_STATIC_LIB
PFN_EXECUTE_CMD s_RunFunc[] =	// 索引应与s_CmdInfo中的命令定义顺序对应
{
	esqlite31_fn_sqlite3_initialize,
	esqlite31_fn_sqlite3_shutdown,
	esqlite31_fn_sqlite3_config,
	esqlite31_fn_sqlite3_libversion,
	esqlite31_fn_sqlite3_libversion_number,
	esqlite31_fn_sqlite3_user_data,
	esqlite31_fn_sqlite3_value_int,
	esqlite31_fn_sqlite3_value_int64,
	esqlite31_fn_sqlite3_value_double,
	esqlite31_fn_sqlite3_value_text,
	esqlite31_fn_sqlite3_value_blob,
	esqlite31_fn_sqlite3_value_type,
	esqlite31_fn_sqlite3_result_text,
	esqlite31_fn_sqlite3_result_blob,
	esqlite31_fn_sqlite3_result_int,
	esqlite31_fn_sqlite3_result_int64,
	esqlite31_fn_sqlite3_result_double,
	esqlite31_fn_sqlite3_result_null,
	esqlite31_fn_sqlite3_result_error,
	esqlite31_fn_sqlite3_result_error_code,
	esqlite31_fn_sqlite3_threadsafe,
	esqlite31_fn_sqlite3_to_gb2312,
	esqlite31_fn_wxsqlite3_config,
	esqlite31_fn_wxsqlite3_config_cipher,
	esqlite31_fn_sqlite3_mutex_enter,
	esqlite31_fn_sqlite3_mutex_leave,
	//数据库
	esqlite31_fn_sqlite3_close,
	esqlite31_fn_sqlite3_close_v2,
	esqlite31_fn_sqlite3_open_v2,
	esqlite31_fn_sqlite3_key_v2,
	esqlite31_fn_sqlite3_rekey_v2,
	esqlite31_fn_sqlite3_exec,
	esqlite31_fn_sqlite3_errcode,
	esqlite31_fn_sqlite3_errmsg,
	esqlite31_fn_get_db,
	esqlite31_fn_set_db,
	esqlite31_fn_sqlite3_prepare_v2,
	esqlite31_fn_sqlite3_last_insert_rowid,
	esqlite31_fn_sqlite3_changes,
	esqlite31_fn_sqlite3_begin_transaction,
	esqlite31_fn_sqlite3_commit_transaction,
	esqlite31_fn_sqlite3_rollback_transaction,
	esqlite31_fn_sqlite3_enum_table,
	esqlite31_fn_sqlite3_enum_index,
	esqlite31_fn_sqlite3_enum_view,
	esqlite31_fn_sqlite3_enum_trigger,
	esqlite31_fn_sqlite3_is_table_exist,
	esqlite31_fn_sqlite3_get_table,
	esqlite31_fn_sqlite3_get_sql,
	esqlite31_fn_sqlite3_create_table,
	esqlite31_fn_sqlite3_delete_table,
	esqlite31_fn_sqlite3_rename_table,
	esqlite31_fn_sqlite3_add_column,
	esqlite31_fn_sqlite3_create_index,
	esqlite31_fn_sqlite3_delete_index,
	esqlite31_fn_sqlite3_create_view,
	esqlite31_fn_sqlite3_delete_view,
	esqlite31_fn_sqlite3_create_trigger,
	esqlite31_fn_sqlite3_delete_trigger,
	esqlite31_fn_sqlite3_vacuum,
	esqlite31_fn_sqlite3_attach_database,
	esqlite31_fn_sqlite3_detach_database,
	esqlite31_fn_sqlite3_backup_database,
	esqlite31_fn_sqlite3_create_function,
	esqlite31_fn_sqlite3_create_agg_function,
	esqlite31_fn_sqlite3_busy_timeout,
	esqlite31_fn_sqlite3_busy_handler,
	esqlite31_fn_sqlite3_db_filename,
	esqlite31_fn_sqlite3_db_readonly,
	esqlite31_fn_sqlite3_db_mutex,
	esqlite31_fn_sqlite3_get_autocommit,
	esqlite31_fn_sqlite3_progress_handler,
	esqlite31_fn_sqlite3_next_stmt,
	esqlite31_fn_wxsqlite3_set_config,
	esqlite31_fn_wxsqlite3_set_config_cipher,
	//记录集
	esqlite31_fn_stmt_destruct,
	esqlite31_fn_stmt_close,
	esqlite31_fn_stmt_set_sql,
	esqlite31_fn_stmt_next,
	esqlite31_fn_stmt_get_text_value,
	esqlite31_fn_stmt_get_int_value,
	esqlite31_fn_stmt_get_double_value,
	esqlite31_fn_stmt_get_int64_value,
	esqlite31_fn_stmt_get_blob_value,
	esqlite31_fn_stmt_reset,
	esqlite31_fn_stmt_data_count,
	esqlite31_fn_stmt_column_count,
	esqlite31_fn_stmt_column_name,
	esqlite31_fn_stmt_column_table_name,
	esqlite31_fn_stmt_column_database_name,
	esqlite31_fn_stmt_column_bytes,
	esqlite31_fn_stmt_column_decltype,
	esqlite31_fn_stmt_column_origin_name,
	esqlite31_fn_stmt_column_type,
	esqlite31_fn_stmt_sql,
	esqlite31_fn_stmt_expanded_sql,
	esqlite31_fn_stmt_get_any_value,
	esqlite31_fn_stmt_bind,
	esqlite31_fn_stmt_bind_parameter_count,
	esqlite31_fn_stmt_clear_bindings,
	esqlite31_fn_stmt_bind_parameter_name,
	esqlite31_fn_stmt_execute,
	esqlite31_fn_stmt_execute_scalar_text,
	esqlite31_fn_stmt_execute_scalar,
	esqlite31_fn_stmt_get_handle,
	esqlite31_fn_stmt_busy,
	esqlite31_fn_stmt_readonly,
		//////////////////////////////////////////////////////////////////////////新增
		esqlite31_fn_aggregate_context,
		esqlite31_fn_context_db_handle,
		esqlite31_fn_sqlite3_total_changes,
		esqlite31_fn_stmt_db_handle,
		esqlite31_fn_stmt_get_line_count,
		esqlite31_fn_sqlite3_create_collation
};

static const char* const g_CmdNames[] =
{
	"esqlite31_fn_sqlite3_initialize",
	"esqlite31_fn_sqlite3_shutdown",
	"esqlite31_fn_sqlite3_config",
	"esqlite31_fn_sqlite3_libversion",
	"esqlite31_fn_sqlite3_libversion_number",
	"esqlite31_fn_sqlite3_user_data",
	"esqlite31_fn_sqlite3_value_int",
	"esqlite31_fn_sqlite3_value_int64",
	"esqlite31_fn_sqlite3_value_double",
	"esqlite31_fn_sqlite3_value_text",
	"esqlite31_fn_sqlite3_value_blob",
	"esqlite31_fn_sqlite3_value_type",
	"esqlite31_fn_sqlite3_result_text",
	"esqlite31_fn_sqlite3_result_blob",
	"esqlite31_fn_sqlite3_result_int",
	"esqlite31_fn_sqlite3_result_int64",
	"esqlite31_fn_sqlite3_result_double",
	"esqlite31_fn_sqlite3_result_null",
	"esqlite31_fn_sqlite3_result_error",
	"esqlite31_fn_sqlite3_result_error_code",
	"esqlite31_fn_sqlite3_threadsafe",
	"esqlite31_fn_sqlite3_to_gb2312",
	"esqlite31_fn_wxsqlite3_config",
	"esqlite31_fn_wxsqlite3_config_cipher",
	"esqlite31_fn_sqlite3_mutex_enter",
	"esqlite31_fn_sqlite3_mutex_leave",
	//数据库
	"esqlite31_fn_sqlite3_close",
	"esqlite31_fn_sqlite3_close_v2",
	"esqlite31_fn_sqlite3_open_v2",
	"esqlite31_fn_sqlite3_key_v2",
	"esqlite31_fn_sqlite3_rekey_v2",
	"esqlite31_fn_sqlite3_exec",
	"esqlite31_fn_sqlite3_errcode",
	"esqlite31_fn_sqlite3_errmsg",
	"esqlite31_fn_get_db",
	"esqlite31_fn_set_db",
	"esqlite31_fn_sqlite3_prepare_v2",
	"esqlite31_fn_sqlite3_last_insert_rowid",
	"esqlite31_fn_sqlite3_changes",
	"esqlite31_fn_sqlite3_begin_transaction",
	"esqlite31_fn_sqlite3_commit_transaction",
	"esqlite31_fn_sqlite3_rollback_transaction",
	"esqlite31_fn_sqlite3_enum_table",
	"esqlite31_fn_sqlite3_enum_index",
	"esqlite31_fn_sqlite3_enum_view",
	"esqlite31_fn_sqlite3_enum_trigger",
	"esqlite31_fn_sqlite3_is_table_exist",
	"esqlite31_fn_sqlite3_get_table",
	"esqlite31_fn_sqlite3_get_sql",
	"esqlite31_fn_sqlite3_create_table",
	"esqlite31_fn_sqlite3_delete_table",
	"esqlite31_fn_sqlite3_rename_table",
	"esqlite31_fn_sqlite3_add_column",
	"esqlite31_fn_sqlite3_create_index",
	"esqlite31_fn_sqlite3_delete_index",
	"esqlite31_fn_sqlite3_create_view",
	"esqlite31_fn_sqlite3_delete_view",
	"esqlite31_fn_sqlite3_create_trigger",
	"esqlite31_fn_sqlite3_delete_trigger",
	"esqlite31_fn_sqlite3_vacuum",
	"esqlite31_fn_sqlite3_attach_database",
	"esqlite31_fn_sqlite3_detach_database",
	"esqlite31_fn_sqlite3_backup_database",
	"esqlite31_fn_sqlite3_create_function",
	"esqlite31_fn_sqlite3_create_agg_function",
	"esqlite31_fn_sqlite3_busy_timeout",
	"esqlite31_fn_sqlite3_busy_handler",
	"esqlite31_fn_sqlite3_db_filename",
	"esqlite31_fn_sqlite3_db_readonly",
	"esqlite31_fn_sqlite3_db_mutex",
	"esqlite31_fn_sqlite3_get_autocommit",
	"esqlite31_fn_sqlite3_progress_handler",
	"esqlite31_fn_sqlite3_next_stmt",
	"esqlite31_fn_wxsqlite3_set_config",
	"esqlite31_fn_wxsqlite3_set_config_cipher",
	//记录集
	"esqlite31_fn_stmt_destruct",
	"esqlite31_fn_stmt_close",
	"esqlite31_fn_stmt_set_sql",
	"esqlite31_fn_stmt_next",
	"esqlite31_fn_stmt_get_text_value",
	"esqlite31_fn_stmt_get_int_value",
	"esqlite31_fn_stmt_get_double_value",
	"esqlite31_fn_stmt_get_int64_value",
	"esqlite31_fn_stmt_get_blob_value",
	"esqlite31_fn_stmt_reset",
	"esqlite31_fn_stmt_data_count",
	"esqlite31_fn_stmt_column_count",
	"esqlite31_fn_stmt_column_name",
	"esqlite31_fn_stmt_column_table_name",
	"esqlite31_fn_stmt_column_database_name",
	"esqlite31_fn_stmt_column_bytes",
	"esqlite31_fn_stmt_column_decltype",
	"esqlite31_fn_stmt_column_origin_name",
	"esqlite31_fn_stmt_column_type",
	"esqlite31_fn_stmt_sql",
	"esqlite31_fn_stmt_expanded_sql",
	"esqlite31_fn_stmt_get_any_value",
	"esqlite31_fn_stmt_bind",
	"esqlite31_fn_stmt_bind_parameter_count",
	"esqlite31_fn_stmt_clear_bindings",
	"esqlite31_fn_stmt_bind_parameter_name",
	"esqlite31_fn_stmt_execute",
	"esqlite31_fn_stmt_execute_scalar_text",
	"esqlite31_fn_stmt_execute_scalar",
	"esqlite31_fn_stmt_get_handle",
	"esqlite31_fn_stmt_busy",
	"esqlite31_fn_stmt_readonly",
		//////////////////////////////////////////////////////////////////////////新增
		"esqlite31_fn_aggregate_context",
		"esqlite31_fn_context_db_handle",
		"esqlite31_fn_sqlite3_total_changes",
		"esqlite31_fn_stmt_db_handle",
		"esqlite31_fn_stmt_get_line_count",
		"esqlite31_fn_sqlite3_create_collation"
};


#endif


/************************************************************************/
/* 数据类型定义
/************************************************************************/

#ifndef __E_STATIC_LIB


LIB_DATA_TYPE_ELEMENT s_dt_element_db[] =
{
	/*{ 成员类型 ,数组成员 , 中文名称 ,英文名称 ,成员解释 ,成员状态 ,默认值}*/
	{ SDT_INT, NULL,_WT("hsqlite"), _WT("hsqlite"), _WT("数据库句柄"), LES_HIDED, 0 },

};

LIB_DATA_TYPE_ELEMENT s_dt_element_stmt[] =
{
	/*{ 成员类型 ,数组成员 , 中文名称 ,英文名称 ,成员解释 ,成员状态 ,默认值}*/
	{ SDT_INT, NULL,_WT("hStmt"), _WT("hStmt"), _WT("记录集句柄"), LES_HIDED, 0 },

};

LIB_DATA_TYPE_ELEMENT s_dt_element_field_info[] =
{
	/*{ 成员类型 ,数组成员 , 中文名称 ,英文名称 ,成员解释 ,成员状态 ,默认值}*/
	{ SDT_TEXT, NULL,_WT("名称"), _WT("name"), _WT("字段名"), 0, 0 },
	{ SDT_INT, NULL,_WT("类型"), _WT("type"), _WT("0、无类型；1、#字节型； 2、#短整数型； 3、#整数型； 4、#长整数型； 5、#小数型； 6、#双精度小数型； 7、#逻辑型； 8、#日期时间型； 10、#文本型； 11、#字节集型； 12、#备注型。"), 0, 0 },
	{ SDT_INT, NULL,_WT("长度"), _WT("length"), _WT(""), 0, 0 },
	{ SDT_INT, NULL,_WT("属性"), _WT("property"), _WT("“SQLITE_字段属性_”开头常量组合"), 0, 0 },
	{ SDT_TEXT, NULL,_WT("默认值"), _WT("default"), _WT("字段的默认值"), 0, 0 },
	{ SDT_TEXT, NULL,_WT("检查"), _WT("check"), _WT("要检查值的条件，如：工资不能为零 “SALARY > 0”；如果条件值为 false，则记录违反了约束，且不能输入到表。"), 0, 0 },
};

INT s_DbCommandIndexs[] =
{
	26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,109,112
};

INT s_StmtCommandIndexs[] =
{
	75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,110,111
};

static LIB_DATA_TYPE_INFO s_DataTypes[] =
{
	/* { 中文名称, 英文名称, 数据描述, 索引数量, 命令索引, 对象状态, 图标索引, 事件数量, 事件指针, 属性数量, 属性指针, 界面指针, 元素数量, 元素指针 } */
	{ _WT("zySqlite数据库"), _WT("zySqliteDB"), _WT("数据库相关操作类，数据库打开后必须手动关闭"), sizeof(s_DbCommandIndexs)/ sizeof(s_DbCommandIndexs[0]), s_DbCommandIndexs, NULL, 0, 0, NULL, 0, NULL, NULL, sizeof(s_dt_element_db) / sizeof(s_dt_element_db[0]), s_dt_element_db },
	{ _WT("zySqlite记录集"), _WT("zySqliteStmt"), _WT("记录集相关操作类，必须手动关闭记录集"), sizeof(s_StmtCommandIndexs) / sizeof(s_StmtCommandIndexs[0]), s_StmtCommandIndexs, NULL, 0, 0, NULL, 0, NULL, NULL, sizeof(s_dt_element_stmt) / sizeof(s_dt_element_stmt[0]), s_dt_element_db },
	{ _WT("zySqlite字段信息"), _WT("zySqliteFieldInfo"), _WT("创建表、添加字段时提供的字段信息"), 0, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, sizeof(s_dt_element_field_info) / sizeof(s_dt_element_field_info[0]), s_dt_element_field_info },
};

#endif

EXTERN_C INT WINAPI  zy_esqlite31_ProcessNotifyLib(INT nMsg, DWORD dwParam1, DWORD dwParam2)
{
#ifndef __E_STATIC_LIB
	if (nMsg == NL_GET_CMD_FUNC_NAMES) // 返回所有命令实现函数的的函数名称数组(char*[]), 支持静态编译的动态库必须处理
		return (INT)g_CmdNames;
	else if (nMsg == NL_GET_NOTIFY_LIB_FUNC_NAME) // 返回处理系统通知的函数名称(PFN_NOTIFY_LIB函数名称), 支持静态编译的动态库必须处理
		return (INT)"zy_esqlite31_ProcessNotifyLib";
	else if (nMsg == NL_GET_DEPENDENT_LIBS) return NULL;
	// 返回静态库所依赖的其它静态库文件名列表(格式为\0分隔的文本,结尾两个\0), 支持静态编译的动态库必须处理
	// kernel32.lib user32.lib gdi32.lib 等常用的系统库不需要放在此列表中
	// 返回NULL或NR_ERR表示不指定依赖文件  
#endif
	return esqlite31_ProcessNotifyLib(nMsg, dwParam1, dwParam2);
};

#ifndef __E_STATIC_LIB
static LIB_INFO s_LibInfo =
{
	LIB_FORMAT_VER,				//库格式号
	_T(LIB_GUID_STR),			//GUID
	LIB_MajorVersion,			//本库的主版本号
	LIB_MinorVersion,			//本库的次版本号
	LIB_BuildNumber,			//构建版本号
	LIB_SysMajorVer,			//所需要的易语言系统的主版本号
	LIB_SysMinorVer,			//所需要的易语言系统的次版本号
	LIB_KrnlLibMajorVer,		//所需要的系统核心支持库的主版本号
	LIB_KrnlLibMinorVer,		//所需要的系统核心支持库的次版本号
	_T(LIB_NAME_STR),			//库名
	__GBK_LANG_VER,				//库所支持的语言
	_WT(LIB_DESCRIPTION_STR),	//库详细解释

#ifndef __COMPILE_FNR			//状态
	/*dwState*/				_LIB_OS(__OS_WIN),
#else
	/*dwState*/				LBS_NO_EDIT_INFO | _LIB_OS(__OS_WIN) | LBS_LIB_INFO2,
#endif	

	_WT(LIB_Author),			//作者
	_WT(LIB_ZipCode),			//邮政编码
	NULL,				//通信地址
	NULL,				//电话号码
	NULL,				//传真
	NULL,				//邮箱地址
	NULL,				//主页
	NULL,				//其他信息

						//自定义数据类型
						sizeof(s_DataTypes) / sizeof(s_DataTypes[0]),
						s_DataTypes,
						//类别说明
#ifndef __COMPILE_FNR
						/*CategoryCount*/   LIB_TYPE_COUNT,			// 加了类别需加此值。
						/*category*/       	_WT(LIB_TYPE_STR),			// 类别说明表每项为一字符串,前四位数字表示图象索引号(从1开始,0无).
						/*CmdCount*/        sizeof(s_CmdInfo) / sizeof(s_CmdInfo[0]),
						/*BeginCmd*/        s_CmdInfo,
#else
						// fnr版本不需要以下信息
						/*CategoryCount*/   0,
						/*category*/        NULL,
						/*CmdCount*/        0,
						/*BeginCmd*/        NULL,
#endif
						/*m_pCmdsFunc*/             s_RunFunc,
						/*pfnRunAddInFn*/			NULL,
						/*szzAddInFnInfo*/			NULL,

						/*pfnNotify*/				zy_esqlite31_ProcessNotifyLib,

						/*pfnRunSuperTemplate*/		NULL,
						/*szzSuperTemplateInfo*/	NULL,

#ifndef __COMPILE_FNR
						/*nLibConstCount*/			sizeof(s_ConstInfo) / sizeof(s_ConstInfo[0]),
						/*pLibConst*/				s_ConstInfo,
#else
						// fnr版本不需要以下信息
						/*nLibConstCount*/			0,
						/*pLibConst*/				NULL,
#endif

						/*szzDependFiles*/			NULL

};

PLIB_INFO WINAPI GetNewInf()
{
	return (&s_LibInfo);
};



#endif


int zySqlite3_exec_callback(void* userData, int nCol, char** datas, char** names)
{
	sqlite3_exec_callback_struct* pData = (sqlite3_exec_callback_struct*)userData;
	if (pData && pData->m_callback)
	{
		return pData->m_callback(pData->m_userData, nCol, datas, names);
	}
	return 0;
}

int zySqlite3_busy_handler_callback(void* userData, int busyCount)
{
	if (userData)
	{
		sqlite3_busy_handler_callback handler = (sqlite3_busy_handler_callback)userData;
		return handler(busyCount);
	}
	return 0;
}

int zySqlite3_progress_handler_callback(void* userData)
{
	if (userData)
	{
		sqlite3_progress_handler_callback handler = (sqlite3_progress_handler_callback)userData;
		return handler();
	}
	return 0;
}

void zySqlite3_xFunc(sqlite3_context* context, int nArg, sqlite3_value** pArgs)
{
	sqlite3_user_data_struct* pData = (sqlite3_user_data_struct*)sqlite3_user_data(context);
	if (pData && pData->m_xFunc)
	{
		pData->m_xFunc(context, nArg, pArgs);

	}
}

void zySqlite3_xStep(sqlite3_context* context, int nArg, sqlite3_value** pArgs)
{
	sqlite3_user_data_struct* pData = (sqlite3_user_data_struct*)sqlite3_user_data(context);
	if (pData && pData->m_xStep)
	{
		pData->m_xStep(context, nArg, pArgs);
	}
}

void zySqlite3_xFinal(sqlite3_context* context)
{
	sqlite3_user_data_struct* pData = (sqlite3_user_data_struct*)sqlite3_user_data(context);
	if (pData && pData->m_xFinal)
	{
		pData->m_xFinal(context);
	}
}

void zySqlite3_xDestroy(void* pApp)
{
	sqlite3_user_data_struct* pData = (sqlite3_user_data_struct*)pApp;
	if (pData && pData->m_xDestroy)
	{
		pData->m_xDestroy(pData->m_userData);
	}
	if (pData)
	{
		delete pData;
	}
}

int zySqlite3_xCompare(void* pApp, int lLen, const void* lData, int rLen, const void* rData)
{
	sqlite3_user_data_struct* pData = (sqlite3_user_data_struct*)pApp;
	if (pData && pData->m_xCompare)
	{
		return pData->m_xCompare(pData->m_userData, lLen, lData, rLen, rData);
	}
	return 0;
}