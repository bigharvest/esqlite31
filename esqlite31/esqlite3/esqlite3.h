#pragma once

#include "sqlite3secure.h"
#include "helper.h"
#include <sstream>

#ifndef __E_STATIC_LIB

#define LIB_GUID_STR "E3ED75AB9F47436EAD9407F4ACB63F14" /*GUID串: {E3ED75AB-9F47-436E-AD94-07F4ACB63F14} */
#define LIB_MajorVersion 1 /*库主版本号*/
#define LIB_MinorVersion 1 /*库次版本号*/
#define LIB_BuildNumber 0 /*构建版本号*/
#define LIB_SysMajorVer 3 /*系统主版本号*/
#define LIB_SysMinorVer 0 /*系统次版本号*/
#define LIB_KrnlLibMajorVer 3 /*核心库主版本号*/
#define LIB_KrnlLibMinorVer 0 /*核心库次版本号*/
#define LIB_NAME_STR "zySqlite3加密支持库" /*支持库名*/
#define LIB_DESCRIPTION_STR "封装wxsqlite3" /*功能描述*/
#define LIB_Author "kyozy" /*作者名称*/
#define LIB_ZipCode "644000" /*邮政编码*/
#define LIB_Address "" /*通信地址*/
#define LIB_Phone	"" /*电话号码*/
#define LIB_Fax		"75602718" /*QQ号码*/
#define LIB_Email	 "" /*电子邮箱*/
#define LIB_HomePage "" /*主页地址*/
#define LIB_Other	"" /*其它信息*/
#define LIB_TYPE_COUNT 1 /*命令分类数量*/
#define LIB_TYPE_STR	"0000基本命令\0" \
						"\0" /*命令分类*/





#define DTP_SQLITE_DB			MAKELONG(1,0)
#define DTP_SQLITE_STMT			MAKELONG(2,0)
#define DTP_SQLITE_FIELD_INFO	MAKELONG(3,0)

#endif


typedef void(_stdcall *sqlite3_backup_callback)(int remaining_page, int total_page);

typedef int (_stdcall *sqlite3_exec_callback)(void* userData, int nCol, char** datas, char** names);

typedef int (_stdcall *sqlite3_busy_handler_callback)(int busyCount);

typedef int(_stdcall *sqlite3_progress_handler_callback)();

typedef void(_stdcall *pxFunc)(sqlite3_context*, int, sqlite3_value**);
typedef void(_stdcall *pxStep)(sqlite3_context*, int, sqlite3_value**);
typedef void(_stdcall *pxFinal)(sqlite3_context*);
typedef void(_stdcall *pxDestroy)(void*);
typedef int(_stdcall *pxCompare)(void*, int, const void*, int, const void*);

//用于创建函数
struct sqlite3_user_data_struct
{
	pxFunc m_xFunc;
	pxStep m_xStep;
	pxFinal m_xFinal;
	pxDestroy m_xDestroy;
	pxCompare m_xCompare;
	void* m_userData;
};

struct sqlite3_exec_callback_struct
{
	sqlite3_exec_callback m_callback;
	void* m_userData;
};

struct sqlite3_busy_handler_callback_struct
{
	sqlite3_busy_handler_callback m_callback;
	void* m_userData;
};

struct sqlite3_progress_handler_callback_struct
{
	sqlite3_progress_handler_callback m_callback;
	void* m_userData;
};

int zySqlite3_exec_callback(void* userData, int nCol, char** datas, char** names);
int zySqlite3_busy_handler_callback(void* userData, int busyCount);
int zySqlite3_progress_handler_callback(void* userData);

void zySqlite3_xFunc(sqlite3_context* context, int nArg, sqlite3_value** pArgs);
void zySqlite3_xStep(sqlite3_context* context, int nArg, sqlite3_value** pArgs);
void zySqlite3_xFinal(sqlite3_context* context);
void zySqlite3_xDestroy(void* pApp);
int zySqlite3_xCompare(void* pApp, int lLen, const void* lData, int rLen, const void* rData);

struct sqlite3_db_struct
{
	sqlite3* hSqlite;
};

struct sqlite3_stmt_struct
{
	sqlite3_stmt* hStmt;
};

#pragma pack(4)
struct sqlite3_field_info
{
	const char* name;
	int type;
	int len;
	int flags;
	const char* defaultValue;
	const char* check;
};
#pragma pack()




#ifndef SETUP_SQLITEDB
#define SETUP_SQLITEDB(x)	sqlite3* hdb = ((sqlite3_db_struct*)x[0].m_int)->hSqlite;
#endif

#ifndef SETUP_SQLITESTMT
#define SETUP_SQLITESTMT(x)	sqlite3_stmt* hStmt = ((sqlite3_stmt_struct*)x[0].m_int)->hStmt;
#endif

#define SQLITE_PRIMARY_KEY		1
#define SQLITE_AUTO_INCREMENT	2
#define SQLITE_NOT_NULL			4
#define SQLITE_UNIQUE			8